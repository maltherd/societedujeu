from aiogram import Bot, Dispatcher, executor, types

API_TOKEN = 'The Token.'

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands=['start', 'help'])
async def help(message: types.Message):
    await message.reply("""
Ceci est le bot du Kartenträgerregister de la Société du Jeu.

/create -> Créer une nouvelle carte (détails seront demandés ensuite)
/modify <fuzzy card name> -> Modifier une carte de façon loggée (détails seront demandés ensuite)
/fix <fuzzy card name> -> Modifier une carte de façon non-loggée (corrections d'ortographe, précisions, reformulations, cleanup...) (aussi loggée pour completeness, mais pas dans le log classique)
/destroy <fuzzy card name> -> Considérer une carte comme détruite (du moins pour le moment...) après confirmation
/cancel -> annuler le flow actuel

Si un <fuzzy card name> est spécifié, la confirmation est toujours demandée avec un bouton C'est juste/Cancel,
et si plusieurs choses sont probables, la désambiguation est proposée.
Il faut que le fuzzy matching prenne en compte la proximité des touches de clavier.

Données :
* Nom de la carte
* Auteur de la carte (nom ou titre personnel, whatever) (raw text)
* Pouvoir de la carte
* Défi associé à la carte
---* Date de la modification/création/destruction (date, default = now)--- (trop chiant avec telegram)
""")

@dp.message_handler(commands=['create'])
async def create(message: types.Message):
    pass
