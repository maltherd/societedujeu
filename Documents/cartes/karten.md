# Kartenträgerregister

## L'Excuse (Tarot)
\textcolor{red}{spielspielspiel}

## Le Jugement (Tarot, 20)
\textcolor{red}{spielspielspiel}

## L'Étoile (Tarot, 17)
\textcolor{red}{spielspielspiel}

## L'Étoile* (ad hoc)
Cette Carte est tacitement attribuée au président en fonction de Stella Valdensis. Pour la gagner, il faut être élu président de Stella Valdensis. Elle se perd lors de la perte des fonctions associées.

## La Tempérance (Tarot, 14)
\textcolor{red}{spielspielspiel}

## Le Responsable de la Perception (ad hoc)
\textcolor{red}{spielspielspiel}

## Le Commissaire de Contrôle aux Règles (ad hoc)
\textcolor{red}{spielspielspiel}

## Le Responsable des Règlementations Lourdes et Chiantes (ad hoc)
\textcolor{red}{spielspielspiel}
