---
geometry: margin=2cm
numbersections: true
documentclass: article
title: "LE COMMENT"
subtitle: "ou Le Manifeste du Parti Commentiste"
---

#. Le Band se porte sur l'épaule droite.
    #. Sauf pour l'ex-président, qui le porte sur l'épaule gauche.
#. Lorsqu'on se ressert, on trinque avec le président.
    #. Si le président est absent, on trinque avec le pourvoyeur de l'alcool resservi.
    #. Il est remarqué que c'est compliqué si chacun achète son verre.
#. Lorsqu'on boit la première gorgée d'un verre (re)rempli, il faut la boire à quelqu'un en lui disant "Je te bois ma *insérer nom de fleur ici*".
    #. Si on a entendu une personne dire ce même nom de fleur, trop peu auparavant, on a perdu.

