---
documentclass: memoir
classoption: [10pt, openany, twoside]
title: Les Règles du Jeu
subtitle: de la Société du Jeu
date: juin 2022
extensions: [raw_tex, raw_attribute]
numbersections: true
papersize: a5
lang: fr
geometry: "top=2cm,bottom=2cm"
---

# AU SUJET DE CE DOCUMENT
Les règles du jeu se veulent fluides, puisque l'on ne saurait ignorer les idées prodigieuses de nos ludiques compagnons. Toute personne présente peut donc proposer une règle ou un changement de règle - on dira alors que c'est les règles «flottantes» qui sont modifiées. Ces dernières ne sont pas écrites. Elles se maintiennent, se tordent et se transforment au gré de l'hystérie collective.

Lorsqu'elles passent l'épreuve de l'utilisation courante «pendant un certain moment», elles sont écrites dans les Règles du Jeu par le comité. Sinon, elles finissent dans l'oubli.


# HIÉRARCHIE
## Joueurs.
a. Ce sont les membres à plein titre de la Société du Jeu.
b. Ils maîtrisent l'art d'intellectualiser l'amusement, et savent que l'humour est quelque chose de sérieux.
c. Leur exubérance les caractérise, mais ils savent la mesurer. (En principe, tout du moins.)
d. Eux seuls portent le Band lors des Parties et des occasions appropriées.
e. Ils sont statutairement membres actifs de l'Association.

## Petits joueurs.
Il s'agit des Joueurs punis par leurs pairs pour leurs excès ou leurs manquements.

## Arbitres.
Un arbitre est parfois explicitement requis dans les règles du Jeu : pour des enjeux, ou pour des jeux à Spielpunkten par exemple. Les conditions pour choisir de qui il s'agit sont parfois indiquées, parfois laissées à l'appréciation des concernés.

Il est aussi possible de saisir un arbitre en cas de litige dans d'autres situations, à l'appréciation de la populace : par exemple pour des Habitudes, ou dans une situation imprévue par les présentes règles du Jeu.

L'arbitre est saisi pour résoudre un litige et dispose de l'autorité pendant la durée de ce litige uniquement.

## Conseil Arbitraire.
L'arbitre, être humain faillible comme ses pairs, peut être contré s'il prend de mauvaises décisions.

Toute personne flouée par un arbitre peut saisir le Conseil Arbitraire, qui est constitué de tous les Joueurs présents sauf l'arbitre et la personne flouée. Il statue sur le cas de la façon qui lui sied, et sa décision peut renverser celle de l'arbitre.

## Commission du Règlement.
Elle se compose d'un à deux joueurs. Sa mission est de faire observer avec attention les règles du Jeu, rappeler aux autres les choses oubliées.

## Spielmeister et Spielmitlaüfer
Le Spielmeister d'une Partie est la personne ayant gagné le plus de Spielpunkten pendant l'Incipit de cette Partie (*c.f.* le point Parties ci-dessous). Le Spielmitlaüfer est le second du même classement. Leur règne débute en même temps que le Grand Jeu, et se termine à la fin de cette Partie.

Ils disposent chacun du droit inaliénable de distribuer autant de gorgées qu'ils ont gagné de Spielpunkten, et même à des non-joueurs. (Ces derniers, n'étant pas tenus à respecter les règles du Jeu, peuvent bien sûr refuser).

Le but de permettre au Spielmitlaüfer de distribuer ses gorgées aussi (et pas seulement le Spielmeister), est d'améliorer le rendement *Spielpunkten gagnés par tous* / *gorgées distribuées au final* de chaque Partie, car la conservation de l'énergie est une bonne chose.

## Paires.
Ce sont deux joueurs qui sont particulièrement proches d'un Pion, et qui le forment aux usages de la Société. Le Pion en question devient leur Protégé.

## Aspirants.
Sont considérés Aspirants ceux et celles qui souhaitent rejoindre la Société, en ont fait la demande, et ne sont pas encore Pions.

## Pions.
a. Un aspirant devient pion dès lors qu'il a complété la seconde étape de son recrutement, et ce jusqu'à ce qu'il soit terminé. Ce temps peut être relativement long, puisque trouver un jeu adéquat et original est une tâche dont la durée ne peut que s'allonger (*c.f.* recrutement ci-dessous).
b. Lorsque l'aspirant devient Pion, les deux Joueurs ayant le plus d'affinités avec lui deviennent ses Paires. Le Pion devient alors leur Protégé.
<!-- c. Lors des Parties, chaque Pion doit porter un pin's à l'image d'un pion (que ce soit d'échecs ou d'un autre jeu), pour annoncer son statut -- statut dont il faut néanmoins être fier ! -->
d. Ils sont également statutairement membres actifs de l'Association.

# RECRUTEMENT
Les profanes peuvent rejoindre la société en passant les trois étapes du recrutement. Trois étant un nombre parfait, on ne saurait accepter quelqu'un en moins de temps.

## Intégration dans les habitudes.
L'aspirant doit participer et réussir à trois Habitudes.

## Beau jeu.
L'aspirant doit participer à des jeux à Spielpunkten ou Enjeux, et donner trois performances admirables. La victoire et la défaite ne sont pas particulièrement prises en compte. Non, c'est le beau jeu, lui, qui est récompensé ! Une fois cette étape franchie, l'aspirant devient Pion.

## Marque personnelle.
Le Pion soumet une proposition de jeu (habitude, jeu à Spielpunkten ou enjeu), à ajouter au Règles du Jeu directement ou à l'Aide-Mémoire. Si la proposition de jeu s'adjoint de règles ou concepts supplémentaires, ils sont également proposés. Cette proposition est ensuite jugée par un comité particulier constitué :

a. des paires du Pion en question,
b. du premier Joueur au classement des Spielpunkten à ce moment, (en cas d'égalité, les paires du Pion tranchent avec adresse et équilibre)
c. d'un Joueur tiré au dé.

Le comité particulier se réunit à huis-clos pendant quelques minutes et délibère de l'intérêt du jeu proposé, de son originalité, et de son alignement avec les valeurs de notre société. Si la propostion est validée, le comité revient à la Partie et annonce la grande nouvelle ! On aura pris soin initialement de convier les membres à cette Partie, car il est adéquat de souhaiter la bienvenue au nouveau Joueur !

Dans le cas malheureux où la proposition n'aurait pas été acceptée, chaque membre du comité particulier boit quelques gorgées pour son opiniâtreté. Le Pion est ensuite informé convenablement des points qui ont failli, et il est invité à recommencer pour une occasion prochaine.

# PARTIES
La Partie est le lieu de rencontre habituel des Joueurs. C'est pendant la Partie que les Règles du Jeu ont cours.

## Déclaration.
a. La Partie est déclarée à l'avance dans le but d'y réunir le plus possible de Joueurs.
b. Il est aussi possible de déclarer une Partie sur le moment si le hasard a réuni suffisamment de Joueurs au même endroit, et que ce n'est pas pendant la réunion habituelle d'une autre Société; c'eût été impoli.
c. Si ce n'est pas le président qui a déclaré la partie, mais qu'il est quand même présent, on peut lui faire boire quelque chose.

## Déroulement.
La Partie se découpe en deux morceaux : L'Incipit et Le Grand Jeu.

### Incipit
a. Au début de l'Incipit, on choisit le(s) membre(s) de la Commission du Règlement.
b. Pendant l'Incipit, on compte les Spielpunkten (*c.f.* jeux à Spielpunkten ci-dessous).

### Passage de l'Incipit au Grand Jeu
a. Lors du passage, on fait le total des Spielpunkten, et on procède aux manoeuvres nécessaires (*c.f.* même endroit).
b. Le moment du passage est décidé par les Joueurs présents, avec l'aide de la commission du règlement. Il faut tenter de déceler le moment le plus tardif possible où les joueurs ont encore un semblant de capacité de compter. En effet, si l'on faisait plus tard, on perdrait le compte des points !

### Grand Jeu
Pendant le Grand Jeu, on ne compte plus les Spielpunkten, notamment parce qu'on en est plus capables, étant donné que c'est lieu de toutes les ivresses.

# CARTES
Titres nomades, les Cartes investissent les Joueurs de pouvoirs spéciaux, et définissent des défis associés. Un Joueur peut posséder plusieurs Cartes à la fois, bien que cela puisse lui en coûter...

## Effigie.
Idéalement, une Carte est représentée par une effigie physique, que ce soit une réelle carte à jouer ou quelque chose d'autre de symbolique, que le détenteur possède les pleins droits de porter avec ou sur lui, lors des parties.

Il ne s'agit donc pas forcément d'une carte au sens commun du terme. En effet, le mot «carte» est un artefact d'une ancienne version du règlement, dans lequel on ne parlait que de cartes de tarot. Nous nous sommes bienheureusement libérés de ce fardeau.

## Gardien des Effigies.
Durant les sombres Interludes, on confiera les effigies à la garde d'un membre spécialement choisi de la Société, afin qu'elles ne se perdent pas. On nommera ce membre Gardien des effigies. Un simple tiroir peut faire office de Gardien, faute de mieux.

## Registre des Cartes.
La Société tient également un livre listant chronologiquement les possesseurs de toutes les Cartes, leurs inventions, chutes en désuétude, reprises, etc, avec un bref explicatif si possible, et autant de précision qu'on réussira à garder. Chaque possesseur d'une Carte est ainsi le n^ème^ possesseur de cette Carte; on peut alors se référer à la personne par cette combinaison («La 3^ème^ Étoile» par exemple) si on arrive à s'en souvenir.

## Création.
La création d'une carte est un procédé simple, qui se veut accessible à toute personne présente suffisamment inspirée. Il suffit qu'un Joueur inscrive la création de la carte dans le registre : son nom, son pouvoir et une ébauche de défi associé. Le Joueur étant doué de discernement, il saura garder les propositions les plus intéressantes ! On ne voudrait pas remplir le registre de cartes vouées à l'oubli.

Le lanceur de la Partie aura pris soin d'apporter du papier et de quoi écrire pour créer les cartes temporaires.

## Abandon.
Le porteur d'une carte peut choisir de l'abandonner. Elle retourne alors au Gardien des Effigies jusqu'à ce qu'elle soit à nouveau portée.

# TITRES
Les titres, aussi appelés titres personnels, sont une manière de référer à un Joueur autre que son prénom. Chaque Joueur n'en a qu'un seul.

## Obtention.
Un Joueur sans titre en gagne un des façons suivantes :

a. Il propose un titre aux autres personnes présentes. S'il est adapté et accepté par ces dernières, le titre est gagné.
b. Il demande formellement aux Joueurs (et non personnes) présents qu'on lui impose un titre parmi trois options.
Il ne peut pas les refuser toutes, mais la populace doit lui proposer quelque chose d'adapté et d'intéressant.
Elle peut chosir de délayer la décision à une partie suivante.
<!-- c. Il le gagne suite à un Défi schwerwiegend. -->


# HABITUDES
«Les Habitudes» est le nom donné à ces jeux qui sont «les nôtres», et qui sont invocables à tout moment. Ils ne s'arrêtent pas à proprement parler.

## Le Jeu.
Le classique. De ce jeu, la subtilité est la prime valeur. Une simple interjection sans contexte disant «Le jeu !» est la trace d'un esprit peu imaginatif, sans intérêt. Il faut travailler son rappel au Jeu, le rendre artistique. Dans notre Société, le receveur doit consentir à avoir été surpris, ou reconnaître la qualité de la phrase dite, pour boire.

## Jeu de mot.
Les calembours et autres jeux de mollets pour cerf-volants sont très appréciés dans notre Société. Lorsque l'on en prononce un très bon, les personnes présentes peuvent boire à son honneur.

## Jeu du dit.
Classique de l'humour de bas étage, sa répétition en fait un chef d'oeuvre inégalable. La personne disant le dit accompagne celles qui l'écoutent en boisson.

De plus, pour punir la méprisable habitude de se redire soi-même, on fera boire le rediseur autant de fois qu'il y a de personnes l'ayant pointé du doigt.

Les personnes observatrices reconnaissant la qualité du dit peuvent boire également, en prononçant la phrase «Dicton bien dicté mérite bidonnage».

## Jeu du double santé.
Il se lance en trinquant «deux fois» d'une traite avec quelqu'un. Le mouvement est précis : il s'agit de choquer avec le haut de son verre puis, rapidement, avec le bas de son verre.

Tout Joueur qui aura laissé son verre seul sur la table se voit au risque de recevoir des double santés face auxquels il ne pourra rien faire. Il peut donc le confier à la garde d'un de ses compagnons dignes de confiance pour s'absenter.

On note qu'un verre n'est pas attribué à un Joueur avant qu'il lui ait porté ses lèvres.

La personne s'étant fait double-santer doit boire autant que gorgées qu'il a reçu de double-santés (son compte est alors remis à zéro).

## Jeu de la rôtisserie.
Inventé pour permettre aux méprisants et autres «vives» langues de rejoindre malgré tout notre Société (car on les y tolère !), ce jeu récompense les plus belles grillades et autres rôtisseries à l'égard d'autres compagnons.

Il convient ici de marcher sur le fil de l'humour sans tomber dans le précipice de l'offense ! La personne recevant la rôtisserie peut boire si elle reconnaît sa qualité.

## Jeu du roi des pouces.
Le roi des pouces est désigné à l'aide d'un jeu quelconque. Il n'est pas obligé d'être un joueur.

S'il pose son pouce sur une surface plane et visible de tous (table par exemple), la course est lancée. Toutes les autres personnes à sa table et/ou qui peuvent clairement voir le pouce doivent poser leur pouce sur la même surface le plus rapidement possible. Le dernier subit un gage.

Il est de bon goût que le roi des pouces n'abuse pas de son privilège de façon trop répétitive.

# JEUX À SPIELPUNKTEN
Les Parties sont ponctuées de jeux divers et variés : jeux de cartes, de mémoire, de paris, etc. Pour les inclure dans le flot général de la Société, il a été décidé que des points spéciaux, les Spielpunkten, seraient à la clé. On nomma alors ces jeux généraux «Jeux à Spielpunkten».

## Obtention
Les conditions d'obtention des Spielpunkten sont variables en fonctions des jeux (en équipe ou chacun pour soi, en plusieurs manches ou non...) et de l'imagination des joueurs.

On peut aussi imaginer que des joueurs assez mauvais doivent rendre leurs points à leurs camarades victorieux. Tout est en effet possible.

## Échelle
Toutefois l'échelle suivante devrait être respectée le mieux possible, quand il s'agit de récompenser la victoire à un jeu (ou manche d'un jeu, d'ailleurs) :

* 1 S.P. correspond à une maigre victoire, pas loin de l'égalité.
* 5 S.P. correspondent à une victoire respectable.
* 10 S.P. correspondent à une très belle victoire !
* 15 S.P. se gagnent lors d'une victoire éclatante !
* 20 S.P., tels le 20/20 français, ne devraient être gagnés sous aucune circonstance normale, telle est la perfection de ce qu'ils récompensent.

On remarque qu'il s'agit ici des Spielpunkten gagnables d'un seul coup. Un joueur décent saura bien sûr accumuler plus que 20 S.P. tout le long d'une Partie.

La raison pour cette échelle est que, comme décrit au point Spielmeister ci-dessus, chaque S.P. se traduit en une gorgée donnée.

## Non-exclusivité
Il serait dommage de se limiter aux Spielpunkten comme récompense ou gage. Une manche de Jass gagnée avec 120 points d'avance se récompense bien avec 10 S.P., mais encore mieux avec 12 gorgées à partager pour les perdants ! (*c.f.* aide-mémoire).

## Aide-mémoire
Nous avons trouvé bon d'écrire séparément un aide-mémoire listant des jeux et des règles associées de boissons et de S.P.. La raison étant que cet aide-mémoire évoluerait plus souvent que les règles ici-présentes, et qu'il serait plus économique de ne pas réimprimer ces dernières à chaque fois.

<!-- ## Défi général
a. Tout jeu conventionnel (jeu de cartes, dés, société...) peut être déclaré un défi. Si le jeu se joue à plus de deux personnes, le nombre de gagnants et perdants change :
    #. Un jeu par équipe a plusieurs gagnants (l'équipe gagnante) et plusieurs perdants (les autres).
    #. Un jeu chacun pour soi n'a qu'un seul gagnant.
b. Les pénalités générales pour les perdants peuvent se compter en gorgées, ou autres gages divers. Les récompenses données aux gagnants se comptent en Spielpunkten, ou en gorgées à distribuer immédiatement.
c. Pour les jeux se jouant par manches, tel que le Jass par exemple, on tolèrera que les pénalités et récompenses soient distribuées à chaque manche, et non seulement à la fin de la partie. -->

<!-- À bouger dans l'aide-mémoire -->
<!-- ## Défi du mot à placer.
a. Un lanceur défie un receveur de placer un mot particulier dans une conversation avec un ou des tiers, sans que ceux-ci ne se rendent compte que c'était pour ce jeu.
b. Un arbitre est choisi et validera le placement correct du mot et donc la victoire du receveur. Des Spielpunkten sont à la clé, où d'autres gages et gorgées.
c. Si la supercherie a été découverte par les tiers, le receveur perd le défi. Alors, la populace en colère s'occupera de lui infliger un sort en gages ou gorgées qui soit adapté. -->

# ENJEUX
Ces jeux-là sont ainsi nommés pour les enjeux particuliers qui sont à la clé.

## Duel de Cartes.
Ces duels mettent en jeu la possession d'une Carte par un Joueur. Ils impliquent un lanceur et un receveur, ce dernier pouvant refuser le duel.

a. D'un commun accord, ils désignent un arbitre.
#. Les personnes ayant un certain nombre de Cartes à leur nom sont considérées comme honorables, et se doivent d'accepter les duels de Cartes.
#. Chaque Carte est soumise à ses propres règles de duel, qui peuvent parfois êtres vagues, mais c'est bien voulu ! Cette imprécision est là pour inspirer les joueurs à fabriquer de plus beaux duels encore : les règles des Cartes peuvent être surchargées à la bonne volonté des joueurs, de l'arbitre, et sans opposition de la foule mirant.
#. Le gagnant du duel remporte la Carte.

<!--
## Défi inspiré par le Titre.
Ces défis sont très similaires aux duels de Cartes, et en reprennent les points ci-dessus, mais diffèrent toutefois en ces points :

a. Le titre d'un Joueur n'a pas de règle de duel prédéfinie. La teneur du défi dépend alors intégralement de l'imagination des personnes présentes pour choisir un défi adapté au titre.
#. L'arbitre peut invalider le défi tel que proposé s'il juge l'imagination des personnes présentes insuffisamment adaptée.
#. Le titre n'est pas mis en jeu. En fait, le titre agit seulement comme inspiration pour trouver une distraction intéressante et personnalisée.

## Défi de Titre schwerwiegend.
Toutefois, un Joueur défié peut élever les enjeux en proposant un «Défi schwerwiegend» («lourd de conséquences») ! Cela indique sa volonté à faire tapis, et à mettre en jeu son titre.

Dans ce cas, pour équilibrer le jeu, le défié demande une faveur (importante !) de son choix au défieur. Pour peu que le défieur accepte l'arrangement, l'affaire est scellée.

a. En cas de victoire du défieur :
    #. Le défié perd son titre.
    #. Le défié peut également consentir (avant de demander la faveur) à ce que son titre soit transmis au défieur plutôt que simplement supprimé. Cela s'appelle faire «Umstieg».
    #. Le défié restera également 2 mois sans titre afin qu'il réfléchisse à tempérer ses ardeurs autodestructrices.

#. En cas de victoire du défié : le défieur doit accomplir la faveur convenue.
#. Lorsqu'un Défi schwerwiegend prend place, sa gravité commande que toutes les personnes présentes s'attroupent pour l'observer; l'arbitre dispose alors de l'autorité pour demander le silence.
#. Il est convenable que le gagnant d'un tel défi reçoive une marque de distinction ad hoc (p.ex. un pin's) et si possible liée à la teneur du défi, pour l'accrocher sur son Band. -->
