---
documentclass: memoir
classoption: [10pt, openany, twoside]
title: Aide-Mémoire
subtitle: de la Société du Jeu
date: juin 2022
extensions: [raw_tex, raw_attribute]
numbersections: true
papersize: a5
lang: fr
geometry: "top=2cm,bottom=2cm"
---

# PAR NOMBRE DE JOUEURS
Vous êtes sept et ne savez pas à quoi jouer ? Voici une table qui vous aidera.

Joueurs|Jeux
------:|:---
$=2$| Crapette rapide, Crapette brésilienne, Scopa à 2, 66
$=3$| Scopa à 3, Défi du mot à placer
$=4$| Belote, Dame de Pique, Jass, Scopa à 4, Tarot à 4,
$=5$| Briscola chiamata, Tarot à 5
$=6$| Jass à 6, Scopa à 6
$\ge 2$| Ascenseur, Gin rummy, Menteur, Uno, 10000
$\ge 3$| Dourak, Dutch, Bizkit
$\ge 4$| Autoroute, Cabana, Président
$\ge 5$| Jeu du Cambodge

# JEUX DE CARTES
## Crapette rapide

## Crapette brésilienne
Nous venant de Gabriel Benato, ce jeu de rapidité à deux personnes est très sympathique.

Joueurs
  : 2

Types de cartes
  : françaises, du 2 à l'As

Type de jeu
  : à manches, de rapidité

a. C'est un jeu à plusieurs manches.
#. Au début de la 1^ère^, les joueurs constituent chacun «leur tas» avec une moitié des cartes, qui ont été mélangées au préalable.
#. Au début de chaque manche, les joueurs prennent leur tas et le pose à leur côté.
#. Ils disposent devant eux une ligne horizontale de 4 cartes de long, faces cachées.
#. Les joueurs vont utiliser cette ligne de quatre cartes pour créer quatre tas inégaux de cartes. Cela se fait ainsi, de gauche à droite :

  1. On dispose trois cartes faces cachées et une carte face visible, puis en repartant de la gauche...
  2. ...deux cartes faces cachées, une carte face visible et rien sur le 4^ème^ tas, puis en repartant...
  3. ...une carte face cachée et une carte face visible, puis...
  4. ...une carte face visible.

# JEUX DE DÉS

# AUTRES JEUX
