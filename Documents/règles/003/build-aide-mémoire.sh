#!/bin/zsh
pandoc -o aide-mémoire.pdf --pdf-engine=xelatex --include-in-header=aide-mémoire-preamble.tex --toc aidemémoire.md
