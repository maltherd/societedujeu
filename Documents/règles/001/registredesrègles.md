---
documentclass: IEEEtran
classoption: [10pt]
title: Le Registre des Règles Standard
subtitle: de la Société du Jeu
date: 06.12.2021
extensions: [raw_tex, raw_attribute]
numbersections: true
indent: true
---

# Méta
Les règles du jeu se veulent fluides, puisque l'on ne saurait ignorer les idées prodigieuses de nos ludiques compagnons. Toute personne présente peut donc solennellement apposer une règle au Registre des Règles Flottantes. Ce dernier est immatériel : il se fonde sur les esprits (critiques et humoristiques) des personnes présentes. Les règles s'y maintiennent, se tordent et se transforment au gré de l'hystérie collective.

Lorsqu'elles passent l'épreuve de l'utilisation courante «pendant un certain moment», elles finissent écrites dans le Registre des Règles Standard par le comité. Sinon, elles finissent dans l'oubli. Les modifications des règles passent par le même processus.
\


# Hiérarchie
## Joueurs
a. Ce sont les membres à plein titre de la Société du Jeu.
b. Ils maîtrisent l'art d'intellectualiser l'amusement, et savent que l'humour est quelque chose de sérieux.
c. Leur exubérance les caractérise, mais ils savent la mesurer. En principe, tout du moins.
d. Eux seuls portent le Band lors des Parties et des occasions appropriées.
e. Ils sont statutairement membres actifs de l'Association.

## Petits joueurs
Il s'agit des Joueurs punis par leurs pairs pour leurs excès ou leurs manquements.

## Aspirants
Sont considérés Aspirants ceux et celles qui souhaitent rejoindre la Société, en ont fait la demande, et ne sont pas encore Goupils.

## Goupils
a. Un aspirant devient goupil dès lors qu'il a complété la seconde étape de son recrutement, et ce jusqu'à ce qu'il soit terminé. Ce temps peut être relativement long, puisque trouver un jeu adéquat et original est une tâche dont la durée ne peut que s'allonger.
b. Lorsque l'aspirant devient Goupil, le Joueur ayant le plus d'affinités avec lui devient son Hôte (appellation notoirement peu claire, s'il en est). Le Goupil devient alors le Protégé de son Hôte.
c. Lors des Parties, chaque Goupil doit porter un pin's à l'image d'un renard, pour annoncer son statut -- statut dont il faut néanmoins être fier !
d. Ils sont également statutairement membres actifs de l'Association.
\

# Recrutement
Les profanes peuvent rejoindre la société en passant les trois étapes du recrutement. Trois étant un nombre parfait, on ne saurait accepter quelqu'un en moins de temps.

## Intégration dans les habitudes
L'aspirant doit participer et réussir à trois jeux continus. Les règles précises des jeux continus ne doivent pas être expliquées; l'aspirant se doit de faire preuve de perspicacité, et les trouver par lui-même !

## Beau jeu
L'aspirant doit participer à des jeux discrets, et donner trois performances admirables. La victoire et la défaite ne sont pas particulièrement prises en compte. Non, c'est le beau jeu, lui, qui est récompensé ! Une fois cette étape franchie, l'aspirant devient Goupil.

## Marque personnelle
Le  Goupil soumet une proposition de jeu, continu ou discret, à ajouter au Registre des Règles Standard directement. Cette proposition est ensuite jugée par un comité particulier constitué de :

a. l'Hôte du goupil en question,
b. le premier Joueur au classement des Spielpunkten à ce moment, (en cas d'égalité, l'Hôte du goupil choisit avec adresse et équilibre)
c. un Joueur tiré au dé.

Le comité particulier se réunit à huis-clos pendant quelques minutes et délibère de l'intérêt du jeu proposé, de son originalité, et de son alignement avec les valeurs de notre société. Si la propostion est validée, le comité revient à la Partie et annonce la grande nouvelle ! On aura pris soin initialement de convier les membres à cette Partie, car il est adéquat de souhaiter la bienvenue au nouveau Joueur !

Dans le cas malheureux où la proposition n'aurait pas été acceptée, chaque membre du comité particulier boit quelques gorgées pour son opiniâtreté. Le Goupil est ensuite informé convenablement des points qui ont failli, et il est invité à recommencer pour une occasion prochaine.
\

# Cartes
## Définition
Titres nomades, les Cartes investissent les Joueurs de pouvoirs spéciaux, et définissent des défis associés. Un Joueur peut posséder plusieurs Cartes à la fois, bien que cela puisse lui en coûter...

## Effigie
Idéalement, une Carte est représentée par une effigie physique, que ce soit une réelle carte à jouer ou quelque chose d'autre de symbolique, que le détenteur possède les pleins droits de porter avec ou sur lui, lors des parties.

Il ne s'agit donc pas forcément d'une carte au sens commun du terme. En effet, le mot «carte» est un artefact d'une ancienne version du règlement, dans lequel on ne parlait que de cartes de tarot. Nous nous sommes bienheureusement libérés de ce fardeau.

## Gardien des Effigies
Toutefois, durant les sombres Interludes, on confiera les effigies à la garde d'un membre spécialement choisi de la Société, afin qu'elles ne se perdent pas. On nommera ce membre Gardien des effigies.

## Kartenträgerregister
La Société tient également un livre listant chronologiquement les possesseurs de toutes les Cartes, leurs inventions, chutes en désuétude, reprises, etc. C'est le Kartenträgerregister. avec un bref explicatif si possible. Chaque possesseur d'une Carte est ainsi le n^ème^ possesseur de cette Carte; on peut alors se référer à la personne par cette combinaison («La 3^ème^ Étoile» par exemple).

## Création
La création d'une carte est un procédé simple, qui se veut accessible à toute personne présente suffisamment inspirée. Il suffit qu'un Joueur inscrive la création de la carte dans le registre : son nom, son pouvoir et une ébauche de défi associé. Le Joueur étant doué de discernement, il saura garder les propositions les plus intéressantes ! On ne voudrait pas remplir le registre de cartes vouées à l'oubli.
\

# Titres
## Définition
Les titres, aussi appelés titres personnels, sont une manière de référer à un Joueur autre que son prénom. En temps normaux, chaque Joueur n'en a qu'un. Du moins, un à la fois.

## Obtention
Un Joueur gagne un titre des façons suivantes :

a. Il propose un titre aux autres personnes présentes. S'il est adapté et accepté par ces dernières, le titre est gagné.
b. Il demande formellement aux Joueurs (et non personnes) présents qu'on lui impose un titre parmi trois options. Il ne peut pas les refuser toutes, mais la populace doit lui proposer quelque chose d'adapté et d'intéressant. Elle peut chosir de délayer la décision à une partie suivante.
c. Il le gagne suite à un Défi schwerwiegend.
\

# Jeux continus
Les jeux continus sont caractérisés par une capacité à être invoqués à tout moment. Ils ne s'arrêtent pas à proprement parler.
On y trouve :

## Le Jeu.
Le classique. De ce jeu, la subtilité est la prime valeur. Un simple interjection sans contexte, «Le jeu !», est la trace d'un esprit peu imaginatif, sans intérêt.  Dans notre Société, le receveur doit consentir à avoir été surpris, ou reconnaître la qualité de la phrase dite, pour boire.

## Jeu de mot.
Les calembours et autres jeux de mollets pour cerf-volants sont très appréciés dans notre Société. Lorsque l'on en prononce un très bon, les personnes présentes peuvent boire à son honneur, en prononçant d'abord la phrase \textcolor{red}{(insérer phrase)}.

## Jeu du dit.
Classique de l'humour de bas étage, sa répétition en fait un chef d'oeuvre inégalable. La personne disant le dit, accompagne celles qui l'écoutent en boisson. De plus, pour punir la méprisable habitude de se redire soi-même, on fera boire le diseur autant de fois qu'il y a de personnes l'ayant entendu. Les personnes observatrices reconnaissant la qualité du dit peuvent boire également, en prononçant la phrase «Dicton bien dicté mérite bidonnage».

## Jeu de la double santé.
Il se lance en trinquant deux fois d'une traite avec quelqu'un. Une course a ensuite lieu, dans laquelle le défiant et le défié se font des double-santés et comptent les scores. Lorsque l'un d'entre eux a deux points d'avance sur l'autre, le premier gagne. Ensuite de celà, pour le reste de la Partie, le perdant s'adresse au gagnant en l'appelant «Monseigneur» (éventuellement suivi du prénom), et le gagnant au perdant avec la formule «Cher serviteur». Si le gagnant oublie d'user de la formule correcte, le perdant peut le faire boire, mais pas l'inverse. Le gagnant a maintenant une galanterie a respecter.

## Jeu de la phrase pugnace.
Inventé pour permettre aux méprisants de rejoindre malgré tout notre Société (car on les y tolère !), ce jeu récompense les plus belles grillades et autres rôtisseries à l'égard d'autres compagnons. Il convient ici de marcher sur le fil de l'humour ! La personne recevant la rôtisserie peut boire si elle reconnaît sa qualité.

# Jeux discrets
Bien que la distinction ne soit pas entièrement claire, nous serons de bons physiciens, et admettrons que les jeux discrets, se déclarent ouverts et s'arrêtent, à l'inverse des jeux continus. La victoire impliquent généralement d'accumuler des Spielpunkten.

À la fin de la Partie, les Joueurs sont classés selon leur total de Spielpunkten, et le 1^er^ est alors désigné Spielmeister pour la Partie suivante uniquement. Lors de celle-ci, il devra alors être traité avec une certaine déférence par les autres Joueurs et Goupils, et il pourra distribuer autant de gorgées qu'il vient de gagner de points (ces gorgées peuvent être attribuées à toute personne présente). Les instances réglementaires adaptées veillent au respect de ceci.

## Jeu du mot à placer.
a. Ce jeu met en scène un lanceur et un receveur.
b. Ils élisent un souffleur parmi les personnes présentes, selon la méthode de leur choix. Le souffleur va alors souffler un mot-clé particulier différent à chacun des deux joueurs.
c. Leur objectif sera de placer ce mot-clé dans une discussion avec l'autre, ou aussi par écrit -- mais il faut que ce soit à la vue et à l'ouïe du souffleur -- avant la fin de la Partie.
d. Le souffleur pourra témoigner de la réussite ou non de chaque participant.
e. Ce jeu peut admettre plusieurs gagnants. Ils gagnent tous deux des Spielpunkten.

## Défi de bière.
\textcolor{red}{Un classique que nous nous devons de revisiter à notre sauce !}

## Défi général
a. Tout jeu conventionnel (jeu de cartes, dés, société...) peut être déclaré un défi. Si le jeu se joue à plus de deux personnes, le nombre de gagnants et perdants change :
b. Un jeu par équipe a plusieurs gagnants et plusieurs perdants.
b. Un jeu chacun pour soi n'a qu'un seul gagnant.
d. Les pénalités générales pour les perdants peuvent se compter en gorgées, ou autres gages divers. Les récompenses données aux gagnants se comptent en Spielpunkten, ou en gorgées à distribuer immédiatement.
\

# Enjeux
Ils sont similaires aux jeux discrets, mais diffèrent en ceci qu'ils ont des enjeux particuliers, au delà des Spielpunkten.

## Duel de Cartes.
Ces duels mettent en jeu la possession d'une Carte par un Joueur. Ils impliquent un lanceur et un receveur, ce dernier devant accepter.

a. D'un commun accord, ils désignent un arbitre. Si aucun accord n'est possible, le responsable de la règlementation lourde et chiante doit endosser le rôle de l'arbitre. (Il dispose toutefois du droit de déléguer cette charge à quelqu'un, car il est responsable).
#. Les personnes ayant un certain nombre de Cartes à leur nom sont considérées comme honorables, et se doivent d'accepter les duels de Cartes.
#. Chaque Carte est soumise à ses propres règles, qui peuvent parfois êtres vagues&nbsp;:&nbsp;c'est voulu&nbsp;! Cette imprécision est là pour inspirer les joueurs à fabriquer de plus beaux duels encore&nbsp;:&nbsp;les règles des Cartes peuvent être surchargées à la bonne volonté des joueurs, de l'arbitre, et sans opposition de la foule mirant.
#. Le gagnant du duel remporte la Carte.

Les Cartes et leurs règles sont inscrites dans une section plus tardive du présent registre.

## Défi inspiré par le Titre.
Ces défis sont très similaires aux duels de Cartes, mais diffèrent en ces points :

a. Le titre d'un Joueur n'a pas de règle associée prédéfinie. Il dépend alors intégralement de l'imagination des personnes présentes pour choisir un défi adapté au titre. (Cela serait rapidement ennuyeux pour le porteur de n'avoir qu'un seul défi lié à son titre, car il change rarement).
#. L'arbitre peut invalider le défi tel que proposé s'il juge l'imagination des personnes présentes insuffisamment adaptée.
#. Le titre n'est pas mis en jeu : il agit alors seulement comme inspiration pour trouver une distraction intéressante et personnalisée.

## Défi schwerwiegend.
a. Toutefois, un Joueur défié peut élever les enjeux en proposant un «Défi schwerwiegend» ! Cela indique sa volonté à faire tapis, et à mettre en jeu son titre. En ce cas, pour équilibrer le jeu, le défié demande une faveur (importante&nbsp;!) de son choix au défieur. Pour peu que le défieur accepte l'arrangement, l'affaire est scellée.
#. En cas de victoire du défieur : le défié perd son titre. Le défié peut également consentir (avant de demander la faveur) à ce que son titre soit transmis au défieur plutôt que simplement supprimé. Cela s'appelle faire «Umstieg». Le défié restera également 2 mois sans titre afin qu'il réfléchisse à tempérer ses ardeurs autodestructrices.
#. En cas de victoire du défié : le défieur doit accomplir la faveur convenue.
#. Lorsqu'un Défi schwerwiegend prend place, sa gravité commande que toutes les personnes présentes s'attroupent pour l'observer; l'arbitre dispose alors de l'autorité pour demander le silence.
#. Il est convenable que le gagnant d'un tel défi reçoive une marque de distinction ad hoc (p.ex. un pin's) à accrocher sur son Band.
