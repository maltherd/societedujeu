---
title: Les Règles du Jeu
subtitle: Spielen wir viel !
header-includes: |
    ```{=latex}
    \newfontfamily\ipa{Doulos SIL}
    ```
---

## Préambule métarèglementaire
La Société du Jeu, ci-après l'Association, s'est ici dotée d'un ensemble de règles. Ce sont les Règles du Jeu.

Simple modification
:   Les Règles du Jeu ont vocation à être fluides et évolutives. À ce titre, elles doivent toujours contenir des dispositions rendant simple leur modification, prolongement ou raccourcissement, par le plus grand nombre.

    Le préambule métarèglementaire est exempt de cette clause, car ce dernier est important pour préserver l'esprit des Règles dans le temps.

Absence de sérieux
:   Les Règles du Jeu, bien que référencées par les Statuts de l'Association, doivent toujours rester bon-enfant et libres de sérieux. Les velléités règlementaires sont les meilleures lorsqu'elles sont contenues.

    De plus, c'est pour cette raison que les Règles peuvent contenir des formulations vulgaires, ou être formulées à la deuxième personne.

Ouverture aux autres
:   Les Règles du Jeu doivent pouvoir être utilisées à des soirées n'impliquant pas seulement des membres de l'Association, et dans lesquelles les personnes ne connaissent pas l'Association. Leur application est alors un bonus amusant à la soirée.

    Cela est plus amusant, permet de dépasser le cadre parfois réduit de l'Association, et permet également d'éviter de l'imposer aux personnes qui n'en veulent pas. C'est pour cela que l'on parle de "soirée" et non de "partie".

    De plus, toujours dans le même esprit d'ouverture, les Règles du Jeu doivent toujours contenir des dispositions permettant l'introduction simple de nouveaux membres. Les charriages à la con sont très rigolos, et donc acceptables pour les pratiquants d'amusement que nous sommes, mais il ne faudrait pas devenir exclusifs outre-mesure.

----

## Définitions
Une personne :
: est définie comme toute personne participant à la soirée.

Les personnes présentes :
: sont toutes les personnes actuellement sur le lieu de la soirée. (Les personnes actuellement parties aux toilettes n'en font donc par exemple pas partie.)

Hôte attitré :
: Chaque personne invitée dans un local aux accès restreints, désigne son hôte attitré parmi les personnes présentes en ayant les accès.

Gorgée :
:   Les quantités d'alcool des Règles sont définies (sauf exception) en gorgées de *J.E.U.* standardisées (*Jurisprudence Éphémère Universelle*).

----

## Règles à la con
(I) Les créateurs des règles à la con étant imparfaits, il est possible d'amender les règles à la con, d'en ajouter ou d'en retirer, au bon vouloir des personnes présentes. En particulier, les punitions en GJEU étant difficiles à équilibrer, elles peuvent être modifiées aussi.


(#) La gorgée de *Jurisprudence Éphémère Universelle* suit ces règles:
    a. Le volume exact d'une gorgée de *J.E.U.* peut être redéfini à tout moment si une majorité des personnes présentes le souhaite. (On précisera qu'on ne sait pas quelle majorité est nécessaire.)
    #. À chaque début de soirée, il faudrait voter un volume exact pour une gorgée de *J.E.U.*, mais puisque c'est pénible, on considère qu'une gorgée de *J.E.U.* correspond par défaut à la quantité suivante : "une gorgée".
    #. À chaque fin de soirée, le volume de la gorgée de *J.E.U.* est réinitialisée à son état par défaut, puisque comme le dit son nom, c'est un standard éphémère.
    #. La gorgée de *J.E.U* est raccourcie en à l'écrit, et en "une gorgée" à l'oral, mais aussi à l'écrit, à cause de la quantité disproportionnée de points et la nécessité de l'écrire en italique.



(#) Si tu entres dans un local restreint avant ton hôte attitré, tu peux lui faire boire 1 GJEU.
(#) Tu peux à tout moment défier une personne présente (de préférence ivre) en duel de prononciation:
    a. Tu commences en prononçant une phrase de la comptine ci-dessous. Si tu échoues déjà, ta superbe doit être punie d'ivresse : la personne défiée peut te faire boire 2 GJEU.
    b. La personne défiée te répond en prononçant la même phrase. Si elle échoue, tu peux lui faire boire 1 GJEU.
    c. La personne défiée entame la phrase suivante, puis tu réponds, et ainsi de suite.
    d. Si les deux duellistes le souhaitent, ils peuvent mettre fin au duel à tout moment sans pénalité.
    e. La comptine est la suivante:

    > Spielen wir viel !  
    Seien wir froh !  
    Wenn du rabiat bist,  
    Trink mit mir gerad'.  

    > Jass ist der Spass !  
    Lumpi mein Hündli.  
    Der Angriff ein Befehl,  
    Aber sowieso wir spielen.  

(#) Si tu fais tomber une carte à travers la fente de ta table, on peut te faire boire 1 GJEU.
(#) Si tu parviens à faire regarder à quelqu'un le contenu du QR de Gilles Baud, tu peux lui faire boire 1 GJEU.
