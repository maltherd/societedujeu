---
title: Le jeu du jeu
subtitle: Spielen wir viel
header-includes: |
    ```{=latex}
    \newfontfamily\ipa{Doulos SIL}
    ```
---

Le présent ensemble de règles à la con s'applique lors des soirées du Jeu. Les règles sont écrites à la deuxième personne parce que c'est moins lourd et plus parlant qu'autre chose.

### Définitions
1. Une "personne" est définie comme toute personne participant à la soirée.
1. Les "personnes présentes" sont toutes les personnes actuellement sur le lieu de la soirée. (Les personnes actuellement parties aux toilettes n'en font donc par exemple pas partie.)
---

Le présent ensemble de règles à la con s'applique lors des soirées du Jeu. Les règles sont écrites à la deuxième personne parce que c'est moins lourd et plus parlant qu'autre chose.

### Définitions
1. Une "personne" est définie comme toute personne participant à la soirée.
1. Les "personnes présentes" sont toutes les personnes actuellement sur le lieu de la soirée. (Les personnes actuellement parties aux toilettes n'en font donc par exemple pas partie.) *** tempus : tant qu'ils n'ont pas quitté la table
1. Chaque personne invitée dans un local aux accès restreints désigne son hôte attitré parmi les personnes présentes ayant les accès.
1. Les quantités d'alcool de cet ensemble de règles sont définies (sauf exception) en gorgées de *J.E.U.* standardisées (*Jurisprudence Éphémère Universelle*).
    a. Le volume exact d'une gorgée de *J.E.U.* peut être redéfini à tout moment si une majorité des personnes présentes le souhaite, ce doit être écrit dans un PV, un PV peut être pris de façon manuscrite. Le volume doit être non-négligeable (au sens physique). (On précisera qu'on ne sait pas quelle majorité est nécessaire, ce peut être la majorité des gens mineurs, sans lunettes, ou whatever.)
    b. À chaque début de soirée, il faudrait voter un volume exact pour une gorgée de *J.E.U.*, mais puisque c'est pénible, on considère qu'une gorgée de *J.E.U.* correspond par défaut à la quantité suivante : "une gorgée".
    c. À chaque fin de soirée, le volume de la gorgée de *J.E.U.* est reset à son état par défaut, puisque comme le dit son nom, c'est un standard éphémère.
    d. La gorgée de *J.E.U* est raccourcie en "une GJEU" ou "un GJEU", prononcé \ipa{[ɡʒøˑ]}, ou simplement "une gorgée" à l'oral.

=> On peut remplacer tout nombre de gorgées par "un cul-sec". Dans les deux sens.

### Règles à la con
1. Les créateurs des règles à la con étant imparfaits, il est possible d'amender les règles à la con, d'en ajouter, au bon vouloir des personnes présentes. La suppression est temporaire. En particulier, les punitions en GJEU étant difficiles à équilibrer, elles peuvent être modifiées aussi.
1. Si tu entres dans un local restreint avant ton hôte attitré, tu peux lui faire boire 1 GJEU, sauf s'il t'a tenu la porte.
1. Tu peux à tout moment défier quelqu'un en lui lançant la devise "Spielen wir viel" au visage. Il doit alors répondre avec la même devise. S'il échoue la prononciation ou ne répond pas, tu peux lui faire boire 1 GJEU. Si toi, tu échoues au tout début, ton ivresse peut être punie par plus d'ivresse ! Le receveur du défi peut te faire boire 2 GJEU.
1. Si tu fais tomber une carte à travers la fente de ta table, on peut te faire boire 1 GJEU.
1. Si tu parviens à faire regarder à quelqu'un le contenu du QR de Gilles Baud, tu peux lui faire boire 1 GJEU.

Spielen wir viel !
Seien wir froh !
Wenn du rabiat bist, trink mit mir gerad'.

Jass ist der Spass !
Lumpi ist mein Hündli !
Der Angriff war ein Befehl,
Santé !
1. Chaque personne invitée dans un local aux accès restreints désigne son hôte attitré parmi les personnes présentes ayant les accès.
1. Les quantités d'alcool de cet ensemble de règles sont définies (sauf exception) en gorgées de *J.E.U.* standardisées (*Jurisprudence Éphémère Universelle*).
    a. Le volume exact d'une gorgée de *J.E.U.* peut être redéfini à tout moment si une majorité des personnes présentes le souhaite. (On précisera qu'on ne sait pas quelle majorité est nécessaire.)
    b. À chaque début de soirée, il faudrait voter un volume exact pour une gorgée de *J.E.U.*, mais puisque c'est pénible, on considère qu'une gorgée de *J.E.U.* correspond par défaut à la quantité suivante : "une gorgée".
    c. À chaque fin de soirée, le volume de la gorgée de *J.E.U.* est reset à son état par défaut, puisque comme le dit son nom, c'est un standard éphémère.
    d. La gorgée de *J.E.U* est raccourcie en "une GJEU" ou "un GJEU", prononcé \ipa{[ɡʒøˑ]}, ou simplement "une gorgée" à l'oral.
