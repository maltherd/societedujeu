### Modification des règles
* Peuvent être faites en live. Les personnes présentes acceptent les idées prodigieuses et les rajoutent à l'ensemble de règles flottantes.
* Les règles flottantes passent l'épreuve de l'utilisation courante ou de l'oubli pendant un petit moment.
* Les règles flottantes sont ensuite écrites dans le registre des règles et deviennent comme les autres.
* En principe, rien n'empêche de supprimer des règles, mais pourquoi voudrait-on le faire ?

### Jeu de recrutement
  * Plusieurs étapes :
    1. Réussir 3 jeux passifs (voir ci-après).
        * On explique à la personne souhaitant nous rejoindre cette règle. On n'explique pas les règles des jeux passifs, la personne doit nous voir faire et s'inspirer.
    1. Participer à 3 défis de carte/titre et avoir une performance admirable à chaque défi (pas forcément gagner, peut-être même perdre, mais il faut avoir du beau jeu).
    2. Créer un jeu.

### Jeux passifs, joués en tout temps
* Le Jeu
  * Consentir à avoir été surpris pour boire
  * Le commissaire aux règles s'occupe des litiges

* Jeu du mot
  * Faire un jeu de mot. Les gens peuvent boire pour honorer le jeu de mot, ils prononcent alors la phrase "??? (inclure un jeu de mot dedans obviously)" ""

* Jeu du di
  * Classique
  * Lorsque l'on s'auto-dit, les personnes présentes boivent, mais le diseur boit autant de fois qu'il y a de personnes présentes.
  * Les personnes reconnaissant la qualité du di peuvent boire, en prononçant la phrase "Dicton bien dicté mérite bidonnage".

* Jeu du double-santés
  * Se lance en faisant un double santé
  * Il faut avoir 2 pts d'avance pour gagner
  * Le perdant doit s'addresser au gagnant en l'appelant "Monseigneur".
  * Le gagnant doit s'addresser au perdant en l'appelant "Serviteur".
  * S'il oublie, il boit.

* Le jeu de la punchline
  * Roaster une autre personne. La personne boit si elle reconnaît le roast. Il faut trouver une phrase qui va bien.

### Jeux actifs
#### Duels de cartes
* Fonctionnement des duels de cartes :
  * Les duels de cartes impliquent un lanceur et un receveur.
  * Arbitre désigné d'un commun accord par les joueurs.
  * Lorsqu'aucun accord n'est possible, le responsable des règles lourdes et chiantes doit jouer l'arbitre, mais il dispose du droit de déléguer cette charge, car c'est un responsable.
  * Une personne ayant un certain nombre de cartes est honorable et se doit d'accepter les duels de cartes.
  * Les règles vagues peuvent être surchargées à la bonne volonté des joueurs et de l'arbitre.


=> Modifications de la suite : on se limite pas au tarot pour les cartes. On peut en créer quand on veut (pas trop souvent quand même parce que sinon c'est un peu compliqué)

0. L'excuse (le Fou, mais osef) [Gester]
  * Permet d'esquiver une conséquence des Règles du Jeu.
  * On transmet le titre lorsque l'on l'utilise.
  * Il faut faire un jeton "Excuse" en 3d ou du style
  * Le président n'a pas le droit de posséder l'excuse.
1. Le bateleur
2. La papesse
  * ...(La constance, la puissance des acquis)
3. L'impératrice
4. L'empereur
5. Le pape
  * ...(Enseignant, rassurant et traditionnel. Guide et conseiller.)
6. L'amoureux
7. Le chariot
  * Celui qui gagne à l'autoroute ? :O
8. La justice
  * c'est trop nul par rapport à "Le jugement"
9. L'ermite
10. La roue de fortune
  * Le duel de cartes est tel : BO3 sur un pile ou face, un jeu de dés au choix, puis un jeu de cartes au choix. Ainsi, le défi lui-même est hasardeux.
11. La force
  * Celui qui gagne un duel de force (bras de fer, etc.)
12. Le pendu
13. La mort
14. La tempérance
  * Celui qui gagne un duel de ne pas boire pendant toute la partie. Le premier qui boit perd.
  * Si une des deux personnes ne boit pas, le défi est de chuchoter toute la partie. Le premier qui est entendu parler à voix haute perd.
15. Le diable
16. La tour (la maison Dieu)
  * Celui qui gagne un jenga. (lol)
17. L'étoile [Yanni]
  * Celui qui gagne un duel en faisant l'action la plus spectaculaire.
18. La lune
19. Le soleil [Pepito]
  * ... (un truc avec la présence imposante)
20. Le jugement [Maxiem]
  * Le détenteur de la carte est toujours arbitre lorsqu'il est présent.
  * On peut défier le détenteur du jugement à un combat d'arbitrage : lorsqu'un arbitre est nécessaire, l'un ou l'autre s'y rend. À la fin de la partie, la populace juge et décide lequel est le meilleur arbitre. Il gagne alors la carte.
21. Le monde

#### Défis liés aux titres personnels
* Les titres personnels ne sont pas transmissibles en principe. (mais en vrai j'ai trop envie que ça change >_>)
* Chaque titre personnel est lié à un défi créativement trouvé et en lien avec le nom du titre. Ce défi est trouvé rapidement après le décernement du titre.
  * Il est possible de défier le détenteur du titre à ce défi (et personne d'autre).
  * Si le défieur gagne, le détenteur du titre ne perd pas son titre, mais il doit ????
* Lorsque quelqu'un n'a aucun titre personnel à son actif (lorsque la personne est nouvelle, ou à la suite d'une malencontreuse circonstance) :
  * Il peut proposer un titre aux autres personnes présentes. S'il est adapté et accepté, le quelqu'un gagne le titre.
  * Il peut formellement demander aux personnes présentes qu'on lui impose un titre. Il ne peut pas le refuser, mais la populace doit proposer quelque chose d'adapté et d'intéressant !

#### Autres défis
* Défi du Jenga
  * faut regarder comment ça  

* Défi du mot à placer
  * A défie B. L'arbitre est nommé, et souffle un mot à A.
  * A doit faire dire le mot à B, avant la fin de la soirée. B  ne connaît pas le mot.

* Défi de bière
  * Il faut descendre sa bière. L'arbitre et la populace déterminent lequel des deux a le mieux réalisé cette action.

* Défi du Faux-Whist
  * Ce défi est chacun-pour-soi. Il y a un vainqueur et de multiples perdants.

* Défi général
  * Tout jeu standard (jeu de cartes, dés, société...) peut-être déclaré un défi.
  * Le défi peut être en équipe, auquel cas il y a plusieurs gagnants et plusieurs perdants; ou il peut-être chacun pour soi, auquel cas il n'y a qu'un seul gagnant.
  * Les conséquences de la victoire ou de la défaite sont alors choisies sur l'instant.

#### Idées supplémentaires
* Truc quand tu oublies de faire santé
* Truc quand
