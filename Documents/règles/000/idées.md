* créer un jeu de cartes / un jenga que l'on peut lancer pendant une partie ; il faut dire une phrase pour le lancer;
  * il faut avoir un système de paris sur le vainqueur
    * l'arbitre prend les paris
    * les N parieurs se mettent d'accord sur la dose à parier

  * jenga:
    * tu poses une brique, qui a un nombre dessus : si tu fais pas tomber, tu distribues, sinon tu bois le nombre de la brique (ou plus lol).
    * la tour dure toute la soirée. tu peux lancer un duel avec qqn pour poser N brique chacun, à tout moment. On choisit donc N dans le défi.
      * Variante : quand tu poses la brique numéro N, tu fais boire à l'autre ce qu'il y a écrit sur la brique (+ N ????? dangereux. 10 tours ça fait 1+2+3+4+5+6+7+8+9+10 = bcp de verres)

  * jeu discret : faire dire un mot clé:
    * A défie B. L'arbitre est nommé, et souffle un mot à A.
    * A doit faire dire le mot à B, avant la fin de la soirée. B ne connaît pas le mot.

* j'ai envie qu'un truc se passe quand tu lances un dé ... on élabore dessus
  * tu peux lancer un d4 pour choisir de multiplier les gorgées ou les pénalités :O pour augmenter les enjeux => ohhhh la tour oulala
  * tu peux lancer un dé pour choisir un jeu parmi les jeux standards

* ce serait cool s'il y avait aussi des jeux discrets : faire un concours de placer "le jeu" ou whatever le premier; bref, quelque chose de pas spectaculaire.

* si tu te di tout seul (comme moi), tous les gens présent boivent, mais toi tu bois autant qu'il y a de gens

*
