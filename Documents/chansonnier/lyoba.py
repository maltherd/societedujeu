ALL_LANGS = ["ch", "fr"]
PUNCTUATION = [",", ".", ":", ";", "?"]

lyrics = [{lang: "" for lang in ALL_LANGS} for i in range(3)]

lyrics[0]["ch"] = """Lè j’armayi di Kolonbètè
Dè bon matin chè chon lèvâ.

Kan chon vinyê i Bachè j’Ivouè
Tsankro lo mè! n’an pu pachâ.

Tyè fan no ché mon pouro Piéro?
No no chin pâ mô l’inrinbyâ.

Tè fô alâ fiêr a la pouârta,
A la pouârta dè l’inkourâ.

Tyè voli vo ke li dyécho?
A nouthron brâvo l’inkourâ.

I fô ke dyéchè ouna mècha
Po ke no l’y pouéchan pachâ.

L’y è j’elâ fiêr a la pouârta
È l’a de dinche a l’inkourâ:

I fô ke vo dyécho ouna mècha
Po ke no l’y puéchan pachâ.

L’inkourâ li fâ la rèponcha:
Pouro frârè che te vou pachâ,

Tè fô mè bayi ouna motèta
Ma ne tè fô pâ l’èhyorâ.

Invouyi no vouthra chèrvinta
No li farin on bon pri grâ.

Ma chèrvinta l’è tru galéja
Vo porâ bin la vo vouêrdâ.

N’ôchi pâ pouêre, nouthron prithre,
No n’in chin pâ tan afamâ.

Dè tru molâ vouthra chèrvinta
Fudrè èpè no konfèchâ.

Dè prindre le bin dè l’èlyije
No ne cherin pâ pèrdenâ.

Rètouârna t’in mou pouro Piéro
Deri por vo on’Avé Maria.

Prou bin, prou pri i vo chouèto
Ma vinyi mè chovin trovâ.

Piéro rèvin i Bâchè j’Ivouè
È to le trin l’a pu pachâ.

L’y an mè le kiô a la tsoudêre
Ke n’avan pâ la mityi aryâ."""

lyrics[1]["ch"] = """Lyôba, lyôba, por aryâ (bis).
Vinyidè totè, byantsè, nêre,

Rodzè, mothêlè, dzouvenè ôtrè,
Dèjo chti tsâno, yô vo j’âryo,

Dèjo chti trinbyo, yô i trintso,
Lyôba, lyôba, por aryâ (bis)."""

lyrics[2]["ch"] = """Lyôba, lyôba, por aryâ (bis).
Lè chenayirè van lè premirè,

Lè totè nêrè van lè dêrêrè
Lyôba, lyôba, por aryâ (bis)."""

lyrics[0]["fr"] = """Les armaillis des Colombettes
De bon matin se sont levés.

Quand ils sont arrivés aux Basses-Eaux
Le chancre me ronge! Ils n'ont pu passer.

Pauvre Pierre, que faisons-nous ici?
Nous ne sommes pas mal embourbés

Il te faut aller frapper à la porte,
A la porte du curé.

Que voulez-vous que je lui dise
A notre brave curé.

Il faut qu'il dise une messe
Pour que nous puissions passer

Il est allé frapper à la porte
Et il a dit ceci au curé:

Il faut que vous disiez une messe
Pour que nous puissions passer

Le curé lui fit sa réponse:
Pauvre frère, si tu veux passer

Il te faut me donner un petit fromage
Mais sans écrémer le lait.

Envoyez-nous votre servante
Nous lui ferons un bon fromage gras.

Ma servante est trop jolie
Vous pourriez bien la garder

N'ayez pas peur, notre curé
Nous n'en sommes pas si affamés

De trop «moler» votre servante
Il faudra bien nous confesser

De prendre le bien de l'Eglise
Nous ne serions pas pardonnés

Retourne-t'en, mon pauvre Pierre
Je dirai pour vous un Ave Maria.

Beaucoup de biens et de fromages vous souhaite
Mais venez souvent me trouver.

Pierre revient aux Basses-Eaux
Et tout le train a pu passer

Ils ont mis le kio à la chaudière
Avant d'avoir à moitié trait"""

lyrics[1]["fr"] = """Lyôba (appel des vaches) pour traire (bis).
Venez toutes, les blanches, les noires,

les rouges, les étoilés sur la tête les jeunes, les autres,
Sous ce chêne où je vous traie,

sous ce tremble où je fabrique le fromage,
Lyôba, lyôba, pour la traite (bis)."""

lyrics[2]["fr"] = """Lyôba (appel des vaches) pour traire (bis).
Les sonnaillères vont les premières,

Les toutes noires vont les dernières.
Lyôba, lyôba, pour la traite (bis)."""

new_lyrics = [{lang: [] for lang in ALL_LANGS} for i in range(3)]

for part in range(len(lyrics)):
    for lang in ALL_LANGS:
        new_lyrics[part][lang] = lyrics[part][lang].split("\n\n")

        for sentence_number in range(len(new_lyrics[part][lang])):
            sentence = new_lyrics[part][lang][sentence_number]
            sentence = zip([""] + list(sentence), sentence, list(sentence[1:]) + [""])

            # Add commas at the end of verses where there aren't
            # if there is a non-punctuation character before this newline, insert a comma
            treated_sentence = []
            for prev, this, next in sentence:
                if prev not in PUNCTUATION and this == "\n":
                    treated_sentence[-1] = (treated_sentence[-1][0], treated_sentence[-1][1], ",")  # fix previous entry, useless for my algo, but OCD commands
                    treated_sentence.append((prev, ",", "\n"))
                    treated_sentence.append((",", "\n", next))
                else:
                    treated_sentence.append((prev, this, next))

            # Remove verse capitals.
            # if there is a newline before this uppercase character, make it lowercase.
            sentence = treated_sentence
            treated_sentence = []
            for prev, this, next in sentence:
                if prev == "\n":
                    treated_sentence[-1] = (treated_sentence[-1][0], "\n", this.lower())  # fix previous entry, useless for my algo, but OCD commands
                    treated_sentence.append(("\n", this.lower(), next))
                else:
                    treated_sentence.append((prev, this, next))

            # Make first letter uppercase
            treated_sentence[0] = (treated_sentence[0][0], treated_sentence[0][1].upper(), treated_sentence[0][2])
            treated_sentence[1] = (treated_sentence[1][0], treated_sentence[1][1], treated_sentence[0][1].upper())  # fix next entry, useless for my algo, but OCD commands

            # Have a dot at the end of the sentence
            if treated_sentence[-1][1] not in PUNCTUATION:
                treated_sentence[-1] = (treated_sentence[-1][0], treated_sentence[-1][1], ".")  # fix previous entry, useless for my algo, but OCD commands
                treated_sentence.append((treated_sentence[-1][1], ".", ""))

            new_lyrics[part][lang][sentence_number] = "".join([pair[1] for pair in treated_sentence]).replace("\n", " ")

    for i in range(len(list(new_lyrics[part].values())[0])):  # assuming all languages have the same number of sentences.
        print(f"> {new_lyrics[part]['ch'][i]}\\")
        print(f"> *{new_lyrics[part]['fr'][i]}*")
        print()

    print()
