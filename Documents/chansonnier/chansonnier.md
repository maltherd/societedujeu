---
documentclass: memoir
classoption: [10pt, openany, twoside]
title: Le Chansonnier
subtitle: de la Société du Jeu
date: avril 2022
extensions: [raw_tex, raw_attribute]
numbersections: true
papersize: a5
lang: fr
geometry: "top=2cm,bottom=2cm,inner=1cm,outer=1.5cm"
---

# Chants d'ici
## Le Ranz des Vaches
*Version de la Fête des Vignerons 1977, qui est raccourcie.*

#### Refrain
> Lyôba, lyôba, por aryâ. *(bis)*\
> *Lyôba, lyôba, pour la traite.*

> Lyôba, lyôba, por aryâ. *(bis, tous ensemble)*\
> *Lyôba, lyôba, pour la traite.*

#### Couplets
> Lè j’armayi di Kolonbètè, dè bon matin chè chon lèvâ.\
> *Les armaillis des Colombettes, de bon matin se sont levés.*

*(refrain)*

> Vinyidè totè, byantsè, nêre, rodzè, mothêlè, dzouvenè ôtrè,\
> *Venez toutes, les blanches, les noires, les rouges, les étoilées sur la tête, les jeunes, les autres,*

> Dèjo chti tsâno, yô vo j’âryo, dèjo chti trinbyo, yô i trintso.\
> *Sous ce chêne où je vous trais, sous ce tremble où je fabrique le fromage.*

*(refrain)*

> Lè chenayirè van lè premirè, lè totè nêrè van lè dêrêrè.\
> *Les sonnaillères vont les premières, les toutes noires vont les dernières.*

*(refrain)*

## Notre Beau Valais
## Mon Beau Chalet
## La Venoge

# Chants de bonne compagnie
## Les Lacs du Connemara
## Le Temps des Cathédrales
## La Bohême
## Capitaine Flam'
## Père Castor
## Les Cités d'Or
## Un morceau d'Emmental
## Santiano
## Il en faut peu pour être heureux

# Chants des sociétés
## Gaudeamus Igitur
## L'Avenir
## Chevaliers de la Table Ronde

# Chants des armées
## L'Internationale
## Le Chant de l'Oignon
## Erika
## Le Chant des Partisans
## The British Grenadiers
## La Strasbourgeoise
