---
title: Société du Jeu
subtitle: Projet de statuts
geometry: margin=2cm
---

# Titre 1 : Dénomination, siège, but

## Article 1 : Nom

La Société du Jeu (ci-après "l'association") est une association à but idéal constituée conformément aux dispositions des articles 60 et suivants du Code civil suisse.

## Article 2 : Siège

Le siège de l'association est à Lausanne.

## Article 3 : Buts

1. L'association a pour buts :
    - l'amusement ;
    - l'amitié ;
    - la promotion et la défaite au Jeu ;
    - le développement des facultés intellectuelles des membres, notamment par la discussion, la science, la culture, l'art.
2. L'association est affranchie de toute orientation de nature politique ou religieuse, puisqu'elle existe pour l'amusement.

## Article 4 : Ressources

Les ressources de l'association se composent :

1. des cotisations de ses membres, si elles existent ;
2. d'éventuels dons ou legs ;
3. du produit de toute activité que l'association peut entreprendre

## Article 5 : Symboles

1. La devise de l'association est "Spielen Wir Viel" ;
2. Les couleurs de l'association sont, jusqu'à changement ultérieur, le Noir et Blanc.

# Titre 2 : Membres

## Article 6 : Qualité de membre

1. Toute personne physique souhaitant contribuer à la réalisation des buts de l'association peut devenir membre de l'association, pour autant que les étudiants universitaires ou de hautes écoles suisses soient et demeurent majoritaires au sein des membres actifs.
2. On distingue trois qualités de membres :
    - Les membres actifs ;
    - Les membres anciens ;
    - Les membres d'honneur

## Article 7 : Acquisition de la qualité de membre

1. L'acquisition de la qualité de membre se fait sur demande, suivant les règlements de l'association.
2. Par sa demande d’admission, le candidat adhère sans réserve aux statuts de l’association et s’engage à respecter les décisions de l’assemblée générale.
3. Tout membre actif devient un ancien lorsqu'il en fait la demande.
4. L'assemblée générale peut nommer des membres d'honneur parmi les membres actifs ou les anciens.

## Article 8 : Perte de la qualité de membre

1. La qualité de membre se pert par la démission ou l'oubli par les membres actifs.
2. Tout membre actif peut démissionner en tout temps de l'association.

# Titre 3 : Organisation

## Article 9 : organes

Les organes de l'association sont les suivants :

1. L'Assemblée Générale (ci après "AG") ;
2. Le Comité ;
3. Les Vérificateurs aux Comptes

# Titre 3.1 : Assemblée Générale

## Article 10 : composition

1. L'AG réunit tous les membres de l'association.
2. Seuls les membres actifs et membres d'honneur disposent du droit de vote à l'AG.
3. Les membres peuvent inviter toute personne à l'AG. Les invités ne disposent pas du droit de vote.

## Article 11 : attributions de l'AG

L'AG est l'organe suprême de l'association. Elle a les compétences suivantes :
    - élire et révoquer les membres du comité ;
    - élire et révoquer les vérificateurs aux comptes ;
    - autoriser la décharge juridique des membres du comité ;
    - décider des activités de l'association en accord avec ses buts ;
    - admettre de nouveaux membres d'honneur occasionnellement ;
    - fixer le montant de l'éventuelle cotisation ;
    - modifier les présents statuts ;
    - approuver le budget, la comptabilité et le bilan annuel ;
    - disposer des actifs sociaux ;
    - dissoudre l'association ;
    - toute autre compétence qui ne soit pas attribuée à un autre organe de l’association

## Article 12 : convocation

1. L'AG se réunit en séance ordinaire une fois par semestre. Elle est convoquée par le comité 15 jours à l'avance par un courriel envoyé aux membres.
2. Une AG extraordinaire est convoquée à la demande du comité, des vérificateurs aux comptes ou d'un cinquième des membres actifs, dans un délai allant de 3 jours de 2 semaines après la demande.
3. La convocation à une AG mentionne sa date, son heure, son lieu, et son ordre du jour. Les documents relatifs aux points portés à l'ordre du jour doivent être consultables au plus tard 3 jours avant l'AG.

## Article 13 : ordre du jour

1. Les points suivants doivent figurer à l'ordre du jour de chaque AGO :
    - élection du comité ;
    - élection des vérificateurs aux comptes ;
    - bilan des vérificateurs aux comptes ;
    - acceptation de la comptabilité et du bilan ;
    - décharge juridique de la présidence ;
    - décharge juridique du comité d'organisation ;
    - acceptation du budget de l'édition
2. L'AG ne peut en aucun cas prendre de décisions en dehors des points mentionnés à son ordre du jour.
3. Tout membre actif peut proposer l'ajout d'un point à l'ordre du jour d'une AG par un courriel envoyé aux membres avant le début de celle-ci. Les ajouts de points doivent être acceptés par l'AG au début de celle-ci.

## Article 14 : représentation

1. Chaque membre actif et membre d'honneur dispose d'une voix à l'AG. En cas d'égalité, un nouveau vote est organisé.
2. L'AG siège valablement si un tiers des membres actifs et membres d'honneur est présent ou représenté. Si ce quorum n'est pas atteint, l'AG est reportée à une date ultérieure et siège alors valablement quel que soit le nombre de membres présents.

## Article 15 : décisions

1. Les décisions de l'AG sont consignées dans un procès verbal, rendu public dans les 30 jours suivant la tenue de l'AG.
2. L'AG prend ses décisions à la majorité absolue des suffrages exprimés.
3. L'AG modifie les statuts ou prononce la dissolution de l'association à la majorité des deux tiers des suffrages exprimés.
4. L'AG prend ses décisions à verre levé, sauf si un cinquième des membres actifs présents demande un vote à bulletins secrets. Les bulletins nuls ne comptent pas comme suffrages exprimés.

# Titre 3.2 : Comité

## Article 16 : membres

1. Le comité est composé d'un président, d'un trésorier, et d'un responsable de l'Éducation au Jeu, qui assume également la charge de secrétaire. Il peut également être composé d'un ou plusieurs vice-présidents, et de tout autre poste suivant l'imagination et la créativité de l'AG.
2. Le comité est élu par l'AG pour un mandat d'un semestre renouvelable.

## Article 17 : compétences

1. Le comité a les compétences suivantes :
    - convoquer l'AG ;
    - administrer l'association ;
    - exécuter les décisions de l'AG ;
    - représenter l’association ;
    - gérer les ressources et le budget ;
    - tenir la caisse ;
    - tenir la comptabilité et le bilan ;
    - veiller au bon fonctionnement de l’association ;
    - sauvegarder les intérêts de l’association ;
    - rapporter son activité à l’assemblée générale.
2. Le comité engage l'association par une signature de deux de ses membres. L'engagement de toute dépense supérieure à 200.- requiert l'approbation de l'AG.

## Article 18 : convocation et réunion

1. Le comité se réunit aussi souvent que nécessaire sur convocation d'un de ses membres.
2. Le comité ne peut délibérer valablement qu'en présence d'un tiers de ses membres.
3. Le comité prend ses décisions tacitement. Un vote peut être demandé par un des membres, auquel cas la décision est prise à la majorité relative des suffrages exprimés.
4. Les décisions prises par le comité sont consignées dans un procès verbal mis à disposition des membres de l'association.

# Titre 3.3 : vérificateurs aux comptes

## Article 19 : membres

Deux vérificateurs aux comptes sont élus par l’AG parmi les membres actifs.

## Article 20 : compétences

Les vérificateurs sont chargés de soumettre à l’AG un rapport sur les comptes qui lui sont présentés. Ils peuvent en tout temps vérifier l’état de la caisse, obtenir la production des livres et pièces comptables, ainsi que convoquer une AG extraordinaire.

## Titre 4 : responsabilité, comptabilité et bilan

## Article 21 : responsabilité personnelle

Tout membre engage sa responsabilité personnelle lorsqu'il cause un dommage intentionnellement ou par négligence grave, en violant les exigences les plus élémentaires de prudence et alors qu'il pouvait se rendre compte qu'il porterait prédjudice à un tiers.

## Article 22 : comptabilité

1. L’association tient une comptabilité et un bilan.
2. Le trésorier présente à l’assemblée générale la comptabilité et le bilan annuel avec le rapport des vérificateurs aux comptes.

# Titre 5 : dissolution

## Article 23 : dissolution

1. En cas de dissolution de l’association, le mandat de liquidation revient aux membres actifs. Il peut être délégué au comité en exercice par un vote à majorité absolue lors de l'AG.

2. L'actif net disponible est entièrement versé à une association d’étudiants ayant des buts similaires à ceux de l’association.

# Titre 6 : dispositions finales

## Article 24

Les présents statuts sont édictés en français et rendus publics. Les présents statuts ont été adoptés par l’assemblée générale du 27 mai 2021 et entrent en vigueur immédiatement.
