---
geometry: margin=2cm
---

![](vialarstyle.svg){width=3cm}

# Assemblée Générale de la Société du Jeu - 14.12.22

## 1. Bienvenue.
L'AG commence autour de 19h20.

Le président Maxime félicite les quelques présents : Arnaud G., Yanni S., Timour J., Maxime J. 

Timour remercie Maxime, et les autres, d'être là.

## 2. Acceptation des éventuels ajouts de points.
Il n'y a pas d'ajouts de points.

## 3. Argent du semestre passé.
### 3a. Rapport de la vérification des comptes.
Yanni montre les comptes à Timour. 

Timour s'exclame que les comptes sont valides.

### 3b. Présentation et acceptation des comptes et du bilan.
Yanni présente les comptes aux membres de l'assemblée.
Il s'agit de 0CHF.

L'assemblée le remercie.

Vote : "Est-ce que l'assemblée accepte les comptes ?"

| Pour | Contre | Neutre |
| -- | -- | -- |
| 4     | 0     | 0     |

## 4. Décharge du comité sortant.

Vote : "Est-ce que l'assemblée accepte la décharge de Maxime J., Louis V., Yanni S. ?"

| Pour | Contre | Neutre |
| -- | -- | -- |
| 4     | 0     | 0     |

## 5. Argent du semestre à venir.

### 5a. Présentation et acceptation du budget

Le budget présenté est de 0 CHF.

Il est noté que ce point devrait être déplacé après l'élection du comité.

Amélie rejoint l'assemblée.

Vote : "Est-ce que l'assemblée accepte le budget ?"

| Pour | Contre | Neutre |
| -- | -- | -- |
| 5     | 0     | 0     |

## 5. Élection du nouveau comité.
Maxime se présente en Président.
Louis V., au loin, se présente en Secrétaire.
Yanni S., se présente en Trésorier.

Vote : "Est-ce que l'assemblée accepte en bloc ces candidats au comité ?"

| Pour | Contre | Neutre |
| -- | -- | -- |
| 5     | 0     | 0     |

## 7. Élection de la vérification des comptes.
Arnaud G. se présente.
Zoé L. est nommée *in absentia*.

Vote : "Est-ce que l'assemblée accepte en bloc ces candidats à la vérification des comptes ?"

| Pour | Contre | Neutre |
| -- | -- | -- |
| 5     | 0     | 0     |

## 8. Points divers.

### 8a. Bands
Maxime J. explique qu'il a avancé CHF 213 pour l'achat d'un gros rouleau de Bands.
CHF 96 lui ont déjà été remboursés. 

Il souhaite transférer une dette de CHF 110 à l'Association plutôt qu'aux membres disparates actuels (et futurs !) qui lui doivent encore de l'argent. (D'ailleurs tous les Bands achetés n'ont pas encore été attribués). 

Le comité peut prendre cette décision à deux, selon Statuts Art.17 al.2, car cette dépense est inférieure à CHF 200. Yanni et Maxime signent cette dépense.

L'Association a maintenant une dette de CHF 110 envers Maxime Jondeau.

### 8b. Manifeste du parti Commentiste.
Arnaud présente son plan de redynamiser les règles du Jeu en les rendant plus simples. Il y a une notion de s'inspirer de ce qui fonctionne déjà dans les autres sociétés. Arnaud pense que la section Recrutement est à garder, car elle a fait ses preuves avec Gabriel et Eva. Arnaud explique que ce serait bien de tester des choses et de voir ce qui reste.

Maxime propose de jeter les concepts d'Arbitre, Commission d'Arbitrage, Commission du Règlement.

Maxime propose de garder les Spielpunkten à des occasions exceptionnelles : LANs et autres... quand on y pense. Il propose que Louis répare le bot, car sinon ce serait sans espoir. Il pense que l'échelle des S.P. est une bonne idée, quoique reformulable.
Il pense que gagner des S.P. à l'issue d'un jeu, c'est évident, et bien, et qu'il faut le garder. 

Le problème est de savoir comment utiliser les S.P... L'idée de s'en servir à la fin de la partie n'est pas top, car c'est la fin et les gens en ont marre. Les utiliser à la partie suivante semble ok ? 

L'assemblée accepte que le S.M. peut distribuer autant de gorgées qu'il a de S.P. à la partie suivante.

Maxime note qu'il faut vraiment qu'on pense à désigner le S.M. à la fin de la partie, lorsqu'on fait usage des S.P.

Il y a une discussion quant à l'utilisation des Cartes. Elle est très faible actuellement, mais Maxime pense que c'est tout de même intéressant et à tenter encore un moment. Arnaud pense qu'il faut les jeter.

Maxime pense que les titres sont à jeter.

Il pense également que les habitudes sont à garder, pas de façon obligatoire, mais de sorte à garder notre autisme écrit quelque part.

Arnaud pense qu'il faut jeter les Enjeux. Maxime également.

### 8c. Tout le monde perd.
Timour annonce à l'assemblée qu'elle a perdu.

### 8d. FLP.
Arnaud annonce qu'il fuck la présidence. Maxime acquiesce.

20h08 fin de l'AG.
