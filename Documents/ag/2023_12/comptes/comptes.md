---
geometry: margin=2cm
---

![](vialarstyle_avec_liseré_couché_simple.svg){width=3cm}

**Société du Jeu - S.W.V!**

\

# Comptes (mai 2023 → déc 2023)

## Bilan

| Actif | Passif | Titre |
| --- | --- | --- |
| 110.- | | Rubans possédés par la SdJ | 
| | 110.- | Dette pour les rubans envers Maxime J. |
| 20.- | | Don de Arnaud G. du 10 mai 2023 |

Bilan: +20.-

## Résultat

| Produit | Charge | Titre |
| --- | --- | --- |
| aucun |
 
Résultat: 0.-

## Signature

Le trésorier: \
\

Lieu et date:
