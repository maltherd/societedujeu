---
geometry: margin=2cm
---

![](vialarstyle_avec_liseré_couché_simple.svg){width=3cm}

**Société du Jeu - S.W.V!**

\

# Convocation

Chers Joueurs,

C'est à nouveau l'heure de l'AG du Jeu, avec plein de votes présent donc ça fait plaisir. 
Elle aura lieu le mercredi 20 décembre 2023 à 19h, en un lieu encore indéfini, qui sera probablement une salle de l'EPFL ou l'étage du Vestibule.
J'encourage des gens à se motiver à me remplacer à la Présidence.  
Voici l'ordre du jour :

1. Bienvenue.
#. Acceptation des éventuels ajouts de points.
#. Rapport d'activité
#. Argent du semestre passé.
    #. Rapport de la vérification des comptes.
    #. Présentation et acceptation des comptes et du bilan.
#. Décharge du comité sortant.
#. Argent du semestre à venir.
    #. Présentation et acceptation du budget.
#. Élection du nouveau comité.
#. Élection de la vérification des comptes.
#. Points divers.
	#. Dette des bands.
	#. Présentation à une date indéfinie sur un thème un peu défini. 

Selon les Statuts Art.13 Al.3 : «Tout membre actif peut proposer l’ajout d’un point à l’ordre du jour d’une AG par un courriel envoyé aux membres avant le début de celle-ci. Les ajouts de points doivent être acceptés par l’AG au début de celle-ci.». Nous accepterons sans doute un message Telegram sur le groupe comme ersatz de courier. Votre proposition devrait contenir le titre et la position numérique du point dans l'OdJ.

Une description est toujours agréable mais pas strictement nécessaire. Il n'est pas clair de si les documents explicatifs de ces points ajoutés (pouvant se faire jusqu'à l'AG) sont soumis à la même deadline de J-3 jours que les documents des points normaux de l'OdJ (voir Statuts Art.12 Al.3).

Additionnellement, les votes se faisant chez nous à verre levé, n'oubliez pas votre verre ! - Pas indispensable si on fait ça au Vestibule.

\

Bien à vous,\
Maxiem aka MVP, président du Jeu,\
S.W.V !
