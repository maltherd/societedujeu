---
geometry: margin=2cm
---

Chers camarades du Jeu,

\

Voici venue une étape importante : la convocation de notre 1ère Assemblée Générale !
Celle-ci se déroulera le jeudi 9 décembre 2021 à 18h30, sur le campus de l'EPFL.

À l'ordre du jour :

1. Bienvenue
1. Modification des couleurs (ou non)
1. Propositions d'évènements et de cotisations associées
1. Propositions de règlements (pas forcément séparés)
    a. Règles du Jeu
    a. Règles de recrutement
1. Argent de 2021
    a. Rapport des vérificateurs des comptes
    b. Acceptation de la comptabilité 2021
    a. Acceptation du bilan 2021
1. Acceptation du budget 2022
1. Point divers
1. (Suite de l') Apéritif

Je vous remercie de bien vouloir vous y présenter, avec votre verre ! (Les votes se faisant à verre levé.)

\

Bien à vous,\
Le président Arnalo\
le jeu
