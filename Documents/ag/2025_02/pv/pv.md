# PV AG SDJ 21.02.25

## Présences

- Joueurs : Maxime, Zoé, Eva, Timour, Arnaud
- Aspirants : Auréliens
- Pions : Alan
- Absents: Gabriel, Yanni, Jean-Daniel, Amélie, Louis Vialar.

## Points

1.  Bienvenue

2.  Acceptation des ajouts de points:

    1. Timour : ajout d'une dépense extra budgétaire : des snacks grâce aux 20CHF de fortune de la SdJ
        *  **Ajout de point accepté à l'unanimité.**

    2. Maxime : modification de la fréquence minimale des assemblées générales.

        * Maxime propose de réduire la fréquence minimale de 2 à 1 par an.
        **Ajout de point accepté à 4 pour et 1 abstention.**

    3. Arnaud : ajout d'un rôle de FM pour correctement gérer l'introduction des nouveaux. (Comme Aurélien, qui est perdu.)

        * **Ajout de point accepté à l'unanimité**

3. Rapport d'activités
    
    * Une conférence chez Eva sur les montres.
    * Une conférence de Gabriel et Maxime sur la Bossa Nova.
    * Une soirée burgers.
    * Une soirée pubquiz.
    * Une sortie cinéma (Le dessin animé Seigneur des Anneaux là).
    * PAS de LAN du Jeu :(

4. Argent du semestre passé : comptes.

    Le trésorier Timour présente les comptes.
    * Entrées : 0CHF.
    * Sorties: 0CHF.
    * Actifs : 130CHF (20CHF de dons Arnaud Gaudard, 110CHF band).
    * Passifs : 110CHF (dette de band envers Maxime).

    Les vérificateurs recommandent l'acceptation des comptes.

    **Les comptes sont acceptés à l'unanimité**

5. Décharge du comité sortant.
    
    Le comité sortant est Présidente: Eva, Tréso: Timour, Yanni: secrétaire.

    **La décharge est acceptée à l'unanimité**

6. Argent du semestre à venir : budget.
    
    Timour présente un budget.
    Nous avons 20CHF d'actifs et la possibilité d'acheter à manger.

    Timour propose d'acheter à manger pour l'assemblée générale au Vestibule.

    Arnaud souhaiterait que l'association s'enrichisse de façon pérenne, ce que les
    joueurs ridiculisent.

    **Le budget est accepté à l'unanimité**

7. Élection du nouveau comité

    Arnaud se présente comme président et promet le changement.

    Timour s'engage comme trésorier et promet de présenter les comptes la veille de la prochaine AG.

    Le secrétaire est désigné : c'est Zoé.

    **Le nouveau comité est accepté à l'unanimité**

8. Élection des vérificateurs des comptes

    Gabriel est désigné volontaire. Eva est volontaire.

    **Les nouveaux vérificateurs sont acceptés à l'unanimité**

9. Points divers

    "vert"

    (tout le monde boit)

    * Maxime rappelle l'existence de la dette des bands.
    Le prix du band est d'à peu près 15CHF. Il reste 7 à 8 bands possibles.

    * Présentations à venir :
        * la présentation "à la con" de Alan est prévue pour ce semestre, maintenant qu'il ne branle plus rien à JI.
        * la présentation sur l'histoire du manga de Maxime, pour laquelle il a accumulé moult matériel de démonstration. Prévue pour ce semestre
        * la présentation de Timour sur ses vacances au Chili (observation stellaire).
        * la présentation de Gabriel avait un projet de parler de son voyage cyclistique en Suisse.
        * la présentation de Timour sur le plus court voyage possible en train qui parcourt tous les cantons.
        * la présentation d'Arnaud sur la télévision analogique et la cassette vidéo.

        Arnaud et Timour sont déjà prêts, ainsi ils visent le printemps.
        Alan et Maxime visent l'été ou la rentrée.

        Timour se vante de son setup home-cinema trop stylé pour les présentations chez lui.

    * Point de Maxime sur la fréquence obligatoire des AG.
        
        Maxime propose de réduire à 1 par année minimum.

        Arnaud, le nouveau président, promet de faire les AG nécessaires, ce qui rend ce point caduc.

        L'aspirant Aurélien décrie le pessimiste nauséabond dont émane la proposition de Maxime.

        **La proposition est votée: 1 pour, 2 blancs, 1 contre.** La majorité absolue n'est pas atteinte pour ce changement de statuts. 

    * Ajout de FM :

        On ne sait plus ce qu'il se passe avec les nouveaux. Il faut que l'on élise un Garde-Fux au comité, et que l'on supprime le "responsable de l'éducation au jeu".

        Le garde-foux (prononcé *fouxe*) aura pour mission de suivre la progression des nouveaux, d'user de menace de louange et de chantage pour les faire venir aux événements,
        ainsi que de les aider un tout petit peu.

        Le garde-fouxe sera responsable de son propre journal, dans lequel il écrira les avancements de nouveaux et leurs choix de pairs.

        **Le rôle est ajouté à l'unanimité**

    * Idée de jeu par Aurélien : si deux personnes souhaitent se servir dans le même plat, ils font un shifumi pour départager lequel commence.

    * Élection du Garde-foux :

        Maxime se présente, car il aime harceler, mais aussi il aime beaucoup les règles de recrutement de notre société.

        Arnaud demande si Maxime soutiendra les règles diverses et variées qui font de notre société ce qu'elle est, afin que les nouveaux les fassent perdurer.
        Il dit que oui.

        **Éléction de Maxime en tant que GF: 4 pour, 1 abstention (lui-même)**

    * Acceptation d'Alan comme Pion
        
        Par la réussite in extremis de son semestre, en même tant que sa présidence réussie à Japan Impact, Timour propose qu'Alan valide sa 3e performance admirable.

        Il avait été (bizarrement) déjà considéré comme Pion!! Les discussions sont confuses, alors on va reconnaître sa performance admirable.

        **La performance est reconnue**.

        Maxime ira rechercher dans le livret rouge les records du passé afin de savoir le niveau exact d'Alan.

        Alan se retire aux WC grâce au mot magique "nigerosaurus".

    * La devise est dite. L'AG est fermée à 20:18.
