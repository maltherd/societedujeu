---
geometry: margin=2cm
---

![](vialarstyle.svg){width=3cm}

**Société du Jeu - S.W.V!**

\

# Convocation

Chers Joueurs,

Notre seconde Assemblée Générale Ordinaire est ici convoquée.
Elle aura lieu le mardi 21 juin 2022 à 19h, à l'EPFL.
Voici l'ordre du jour :

1. Bienvenue.
#. Acceptation des éventuels ajouts de points.
#. Proposition de cotisation exceptionnelle pour avoir de l'hydromel à l'AG (Timour).
#. Argent du semestre passé.
    #. Rapport de la vérification des comptes.
    #. Présentation et acceptation des comptes et du bilan.
#. Décharge du comité sortant.
#. Argent du semestre à venir.
    #. Présentation et acceptation du budget.
#. Élection du nouveau comité.
#. Élection de la vérification des comptes.
#. Proposition de modification des règlements (Arnaud).
#. Proposition d'achat groupé de rubans avec de l'argent personnel (Arnaud).
#. Rappels des missions en cours pour ne pas oublier (Arnaud).
    #. Maxime : achat d'un carnet pour le registre des cartes.
    #. Zoé : design des cartes.
#. Points divers.

Je vous rappelle que, selon les Statuts Art.13 Al.3 : «Tout membre actif peut proposer l’ajout d’un point à l’ordre du jour d’une AG par un courriel envoyé aux membres avant le début de celle-ci. Les ajouts de points doivent être acceptés par l’AG au début de celle-ci.». Nous accepterons sans doute un message Telegram sur le groupe comme ersatz de courier. Votre proposition devrait contenir le titre et la position numérique du point dans l'OdJ.

Une description est toujours agréable mais pas strictement nécessaire. Il n'est pas clair de si les documents explicatifs de ces points ajoutés (pouvant se faire jusqu'à l'AG) sont soumis à la même deadline de J-3 jours que les documents des points normaux de l'OdJ (voir Statuts Art.12 Al.3).

Additionnellement, les votes se faisant chez nous à verre levé, n'oubliez pas votre verre ! Si vous n'en avez pas, un verre pourra vous être fourni sur demande directe au 3ème tiroir depuis le bas de l'armoire droite de la salle des commissions. Il faudra simplement le laver après utilisation.

J'espère que vous saurez me pardonner le petit retard d'une demi-nuit,

\

Bien à vous,\
Arnalo, président du Jeu,\
S.W.V !
