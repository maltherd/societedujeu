---
geometry: margin=1.5cm
title: Procès verbal de l'assemblée générale du 21 juin 2022
date: 21 juin 2022
toc: true
tot-title: Ordre du Jour
number-sections: true
numbersections: true
header-includes: |
    \usepackage{graphicx}
---

\newpage

Début à 19h20.

# Bienvenue.

Le président en exercice, Arnaud G., souhaite la bienvenue à l'assemblée.

Maxime J. informe l'assemblée qu'il n'a pas lu l'ordre du jour.

# Acceptation des éventuels ajouts de points.

Timour J. demande l'inversion des points (3) ("Proposition de cotisation...") et (4) ("Argent du semestre passé"). Cette demande est acceptée à verre levé (et reflétée dans ce PV)

# Argent du semestre passé.

## Présentation des comptes et du bilan

La société a contracté un achat extra-budgétaire d'Hydromel (pour une valeur de 30 francs suisses, ce qui est inférieur à la limite statutaire de 200 francs suisses).

La société dispose donc de 40 francs de marchandises (sous forme de 2 bouteilles d'hydromel) et de 30 francs de dette envers Arnaud G.

Des questions sont soulevées par rapport à cette dépense extra-budgétaire et ses potentielles conséquences (il semblerait que si l'AG venait à refuser de mettre en place une côtisation, la société pourrait se trouver en faillite).

## Rapport de la vérification des comptes

Amélie S. a réalisé la vérification des comptes, le second vérificateur étant absent. Elle salue le travail du trésorier, tout en déplorant une dépense qui bien qu'inférieure aux limites statutaires se fait avec un budget et une caisse de zéro francs, ce qui place les membres dans une situation inconfortable.

Arnaud G. rassure l'assemblée: si elle choisit de ne pas voter une côtisation pour renflouer la société, les bouteilles resteront fermées et leur valeur de revente sera réalisée pour remettre les comptes de la société à zéro.

## Acceptation des comptes

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 4 | 1 | 1

 Table: Vote: Acceptation des comptes

Les comptes sont acceptés à la majorité absolue.

# Proposition de cotisation exceptionnelle pour avoir de l'hydromel à l'AG (Timour).

Timour J. propose que les membres présents à l'AG acceptent de diviser entre eux le prix des deux bouteilles d'hydromel afin d'éponger la dette mentionnée préalablement.

Taquin, Louis V. fait remarquer que si la côtisation ne s'applique pas à tous les membres, il s'agit en réalité d'un don.

Après délibération, l'intitulé proposé au vote est le suivant: l'AG doit accepter de vendre pour 30 francs l'hydromel détenu par l'association aux personnes présentes lors de cette AG, qui s'engagent à payer l'association pour cet achat (et s'organisent indépendament pour effectuer ce remboursement).

Il est confirmé que les personnes ne consommant pas d'Hydromel n'auront pas à prendre part à un quelconque remboursement.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 6 | 0 | 0 |

 Table: Vote: Vente de l'Hydromel

**Le point est accepté.**

# Décharge du comité sortant.

Amélie S. demande ce qu'il se passe si aucune personne n'est élue à un poste obligatoire.

Amélie S. demande à ce que le point soit amendé afin que les postes de président et trésoriers soient déchargés à la condition qu'un nouveau président et trésoriers soient acceptés au point "Élection du nouveau comité" (dans le cas contraire, les tenants actuels de ces postes seront reconduits automatiquement).

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 8 | 0 | 0

 Table: Vote: Amendement d'Amélie S

**L'amendement est accepté.**

Louis V. demande un rapport d'activités du comité.

Arnaud G. indique que le comité a organisé des parties et activités, et a travaillé sur les documents et règlements de la société.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 6 | 2 | 0

 Table: Vote: Décharge du comité

Aux cris de "Fuck la présidence", Maxime J. et Mathieu G. votent "Contre" ce point.

**Le comité est déchargé.**

# Argent du semestre à venir.

Timour J. présente un budget dans lequel la dette de l'association est résolue par la vente des deux bouteilles d'Hydromel.

Louis V. propose une côtisation modeste de 10 ou 15 francs suisses par année pour financer des achats de bien commun, tels des bands ou des boissons à l'Assemblée Générale.

Il y a une opposition forte à cette proposition de la part de certains membres qui indiquent qu'ils démissionneraient.

Face à cela, Louis V. propose simplement au nouveau trésorier de préparer pour la prochaine AG deux budgets, un avec et un sans côtisation, afin de montrer aux membres à quoi leur côtisation pourrait être utilisée. La proposition de côtisation est remise à plus tard.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 8 | 0 | 0

 Table: Vote: Acceptation du budget

Mathieu G. "Fuck la présidence".

# Élection du nouveau comité.

Les postes à pourvoir sont: _Président_, _Trésorier_, _Responsable de l'Éducation au Jeu (Secrétaire)_. 

Postes facultatifs: Vice-président(s), autres postes.

Louis V. candidate au poste de secrétaire.

Yanni S. candidate au poste de trésorier.

Maxime J. candidate au poste de président.

Yanni S. demande quelle est la majorité requise à l'élection. Arnaud G. indique qu'il s'agit de la majorité absolue des suffrages exprimés.

Amélie S. demande si le cumul des postes est possible. Arnaud G. indique que ce n'est pas règlementé.

Plusieurs personnes proposent Arnaud G. aux postes de président et vice président. Ce dernier refuse et sa candidature n'est donc pas prise en compte.

 | Poste | Personne | Pour | Contre | Neutre |
 |-------|----------|:-:|:-:|:-:|
 | Président | Maxime J. | 4 | 2 | 2
 | Trésorier | Yanni S. | 6 | 1 | 1 |
 | Secrétaire | Louis V. | 8 | 0 | 0

Maxime J. n'obtient pas la majorité absolue et n'est donc pas élu. Arnaud G. reste maintenu au poste de président. Il indique son mécontentement et le fait qu'il démissionnera à la fin de l'assemblée générale.

**Yanni S. et Louis V. sont élus au comité en tant que Trésorier et Secrétaire. Arnaud G. est reconduit au poste de Président.**

# Élection de la vérification des comptes.

Louis V. souhaite que Amélie S. et Zoé L. soient élues, car elles ont fomenté pour forcer le président en poste à être maintenu alors qu'il ne souhaitait pas. Zoé L. ayant été embriguée de force dans cette combine, il corrige et propose Timour J. à la place.

Yanni S. propose de modifier l'intitulé du point en "Désignation des vérificateurs aux comptes".

Amélie S. demande la majorité requise pour un amendement. Arnaud G. répond qu'il s'agit d'une majorité absolue.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 7 | 1 | 0

 Table: Vote: Amendement du point

Timour J. propose Simon G. car il devait être présent et n'est finalement pas présent.

Maxime J. propose Louis D. pour la même raison.

Nous procédons à un vote à N - 1 tours parmi les quatre propositions. 

Timour J. et Amélie S., représentant plus d'1/5 des membres, demandent un vote à bulletins secrets.

Les autres membres de l'assemblée, majoritairement ennuyés par cette proposition, enjoignent à Timour J., fauteur de troubles, d'aller chercher du papier.

Candidats (à la désignation):

 - Louis D.
 - Amélie S.
 - Zoé L.
 - Simon G.
 - Timour J.
 
Votes au premier tour: 

 - Louis D.: |||
 - Amélie S.: ||||||
 - Zoé L.. |
 - Simon G.: |||
 - Timour J.: ||||

Zoé est éliminée

 - Louis D.:    ||
 - Amélie S.:   |||||
 - Simon G.:    ||
 - Timour J.:   |||||
 
Vu la présence d'une majorité absolue pour deux candidats uniquement, Amélie S et Timour J sont _désignés_ vérificateurs des comptes.

# Interlude : proposition d'ajout de point (Amélie S.)

Amélie S. propose de rajouter un point (se basant sur l'expérience passée qui permet de modifier un OdJ accepté DURANT l'assemblée générale) consistant en la modification des statuts pour créer une commission de contrôle chargée de la bonne application de ceux ci.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 4 | 3 | 1 |

 Table: Vote: Ajout du point à l'ordre du jour

La majorité absolue n'est pas atteinte, ce point n'est pas discuté.

Louis V. propose à la proposante de proposer un point à la prochaine assemblée générale.

# Proposition de modification des règlements (Arnaud).

Arnaud G. présente le règlement modifié lors des dernières séances. Il rappelle qu'il n'est pas obligatoire de voter pour accepter ce nouveau règlement. Il souhaite que ce point soit utilisé comme forum pour discuter des règlements et d'éventuelles suggestions de changements.

L'AG a la flemme de discuter du règlement, les gens intéressés en discuteront plus tard.

# Proposition d'achat groupé de rubans avec de l'argent personnel (Arnaud).

Avoir un ruban étant vachement stylé, Arnaud G. propose l'achat de rubans jolis et stylés pour pouvoir les porter.

Qui est intéressé (jusqu'à EUR 24):

 - Zoé
 - Louis
 - Maxime 
 - Arnaud
 - Timour
 - Gabriel (une fois Joueur)
 - (Absent) Louis D.

(Total: EUR 168)
 
Autres intérêts:

 - Amélie (jusqu'à EUR 12)

(Total: EUR 012)

Pas intéressés:

 - Yanni S.
 - Mathieu 

Total: EUR 180

En ajoutant EUR 51 nous pourrions doubler la quantité de Bandër acheté. Louis propose de subventionner partiellement. Si on part sur 1.5m par personne environ, les frais pour chaque nouveau membre seraient de 12€ pour obtenir un band, payé en priorité à Louis puis aux autres souscripteurs de manière équitable.

Option alternative: les 7 intéressés mettent 33€ chacun, et les nouveaux membres (incluant Amélie) payent CHF 12.6 répartis entre les 7 intéressés (1.80 chaque).

Les membres intéressés par le ruban discuteront en dehors de cette AG des modalités tarifaires et de l'éventuelle subvention pour nouveaux membres.

# Rappels des missions en cours pour ne pas oublier (Arnaud).

 - Maxime : achat d'un carnet pour le registre des cartes.
 - Zoé : design des cartes.

# Points divers.

## Fuck la présidence (Mathieu G)

C'est tout.

## Comptes (Yanni S.)

Qui a consommé de l'Hydromel?

S'ensuivent des discussions sur la gestion des dépenses. Yanni S. créera un Tricount.

## Grillades à Épalinges (prochaine partie) (Maxime J.)

29 ou 30 juin, telle est la question.

 - 29 juin: 6
 - 30 juin: 6

Égalité, Maxime décide. **Le 30 Juin au soir.**

_Fin de l'assemblée à 21h12._