files=${@:1}

target=.~pandocpreview.pdf

pandoc --citeproc --template=./template.tex -s -o $target $files

okular "$target" &

inotifywait -q -m -e close_write $files |
while read -r filename event; do
  echo "Building $files to $target..."
  pandoc --citeproc --template=./template.tex -s -o $target $files
  echo "Done"
  echo ""
done

