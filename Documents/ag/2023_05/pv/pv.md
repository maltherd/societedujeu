---
geometry: margin=1.5cm
title: Procès verbal de l'assemblée générale du 10 mai 2023
date: 10 mai 2023
toc: true
tot-title: Ordre du Jour
number-sections: true
numbersections: true
header-includes: |
    \usepackage{graphicx}
---

\newpage

1. Bienvenue.
#. Acceptation des éventuels ajouts de points.
#. Argent du semestre passé.
    #. Rapport de la vérification des comptes.
    #. Présentation et acceptation des comptes et du bilan.
#. Décharge du comité sortant.
#. Argent du semestre à venir.
    #. Présentation et acceptation du budget.
#. Élection du nouveau comité.
#. Élection de la vérification des comptes.


Début à 19h31.

# Bienvenue.

Le président en exercice, Maxime J., souhaite la bienvenue à l'assemblée.

Face à la pénurie chronique de verres du jeu, faute à un climat géopolitique notoirement instable, il est exceptionnellement autorisé de voter à canette ou bouteille levée.

# Acceptation des éventuels ajouts de points.

Arnaud G. propose à ce qu'un point soit ajouté pour promouvoir Aurélien H. au rang d'Aspirant.

**Majorité suffisante.**

# Argent du semestre passé.

## Présentation des comptes et du bilan

Arnaud G. et Zoé L. n'ont pas fait leurs devoirs.

Louis V. propose que l'AG rejette les comptes, étant donné que les vérificateurs n'ont manifestement pas fait leur travail.

Après rapide concertation entre les vérificateurs et le caissier, il est proposé d'accepter les comptes "manifestement de bonne facture".


## Présentation et acceptation des comptes et du bilan

Yanni S. présente les comptes.

Arnaud G. demande quel est le bénéfice et quelles sont les dépenses.

Yanni S. répond que le bénéfice et les dépenses ce semestre sont de 0.

Arnaud G. souhaite un point de situation sur le bilan de l'association.

Le bilan est composé de 110 francs d'actifs (le ruban) compensés par 110 francs de dette à l'égard de Maxime J..

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 4 | 4 | 0

 Table: Vote: Acceptation des comptes

**Les comptes et le bilan sont refusés.**

En conséquence, il sera nécessaire de re-présenter les comptes à la prochaine AG.

Maxime J. demande pourquoi les comptes ont été refusés.

Louis V. répond que l'impréparation manifeste des vérificateurs aux comptes et du trésorier concernant l'état des comptes ne permet pas de valider sérieusement les comptes tels qu'ils ont été présentés.


# Décharge du comité sortant.

Pour rappel, le comité est composé de :

 - **Président** : Maxime J.
 - **Secrétaire** : Louis V.
 - **Trésorier** : Yanni S.

Arnaud G. demande un rapport d'activités au comité.

Maxime J. répond que le comité a organisé plusieurs événements : 

 - Soirée Ramen chez Mathieu ;
 - Partie de jeux de sociétés à Renens, au succès relatif ;
 - Partie de cartes à Renens ;
 - Présentation à la Maison S! ;
 - Grillades.

Le semestre a été entrecoupé de gros événements organisés par une autre association qui ont monopolisé le temps libre des membres.

Amélie S. observe qu'il serait étrange de décharger le comité alors que ses comptes n'ont pas été acceptés.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 2 | 6 | 0

 Table: Vote: Décharge du comité

**Le comité n'est pas déchargé.**

Le comité devra donc être déchargé à la prochaine assemblée générale.

# Argent du semestre à venir.

Le budget présenté comprend 0 francs de dépenses et 0 francs d'entrées.

Arnaud G. propose d'instaurer des côtisations libres.

Louis V. signale de manière sarcastique que des côtisations libres s'appellent des "dons".

Arnaud G. propose d'institutionnaliser les dons, de lancer un mouvement de dépot d'argent.

Louis V. signale qu'il n'est pas vraiment pertinent de demander des côtisations avant d'avoir un budget de dépenses correspondant à ces côtisations.

Une discussion partiellement hors sujet s'ensuit.

Arnaud G. propose de voter pour qu'on rende officielle la possibilité de donner l'argent à la SdJ pour rendre ses activités plus grandioses. Bien que cette possibilité existe depuis toujours, personne ne l'a jamais appliquée.

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 5 | 2 | 0

 Table: Vote: Proposition d'Arnaud

La proposition est acceptée. _Rien ne se passe._

Arnaud annonce qu'il donne 20 francs. _Rien ne se passe._

 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 7 | 0 | 0

 Table: Vote: Acceptation du budget

**Le budget est activé.**

Arnaud G. souhaite que la prochaine fois les points soient faits dans le bon ordre.

# Élection du nouveau comité

Les postes à pourvoir sont: _Président_, _Trésorier_, _Secrétaire_. 

Louis V. candidate au poste de secrétaire.

Arnaud G. présente le cahier des charges du président, mais est interrompu par des cris de "Fuck la présdience !".

Maxime J. candidate au poste de président. Arnaud G. tente de convaincre Amélie S. de candidater, sans grand succès. Amélie S. ne refuse pas la candidature et est donc officiellement candidate au poste.

Arnaud G. candidate au poste de trésorier.

Pour le vote du président, Louis V. propose de faire deux rounds de vote : un round pour accepter les candidats à la majorité absolue, suivi d'un vote de préférence dans le cas où plusieurs candidatures seraient acceptées.

Pour éviter cette procédure longue et délicate, les candidats proposent de départager leur candidature au Feuille, Cailloux, Ciseaux en Bo3 ; le perdant s'engageant à retirer sa candidature.

 - Première manche : MAXIME J.
 - Seconde manche : AMÉLIE S.
 - Troisième manche : MAXIME J.

Suite à ce duel, Amélie S. retire sa candidature.

 | Poste | Personne | Pour | Contre | Neutre |
 |-------|----------|:-:|:-:|:-:|
 | Président | Maxime J. | 5 | 2 | 1 |
 | Trésorier | Arnaud G. | 6 | 1 | 1 |
 | Secrétaire | Louis V. | 6 | 2 | 0 |

Compteur de "Fuck la présidence" : 2

Compteur de "Fuck arnaud" : 1

**Maxime J., Arnaud G. et Louis V. sont élus au comité en tant que Président, Trésorier et Secrétaire, respectivement.**

# Élection de la vérification des comptes.

L'assemblée propose que les deux candidats soient choisis par un tirage aléatoire aux cartes.

Maxime J. choisit l'atout Pique.

Les cartes obtenues sont :

 - Zoé L. : 6 de trèfle
 - Amélie S. : Rois de trèfle
 - Gabriel B. : Valais de trèfle
 - Pepito D. : Nel
 - Yanni S. : 6 de coeur.

Amélie S. et Pepito D. sont donc candidats au poste de contrôleur des comptes.


 | Pour | Contre | Neutre |
 |:-:|:-:|:-:|
 | 6 | 2 | 0 | 

 Table: Vote: Acceptation des vérificateurs aux comptes

# Point Ajouté : Promotion de Aurélien H. au rang d'Aspirant

Louis V. observe qu'il n'est pas nécessaire que l'AG se prononce sur cette proposition.

Arnaud G. souhaitait entendre les motivations d'Aurélien H., présent ce soir, qui a indiqué sa motivation à rejoindre la société.

Aurélien H. : (retranscription approximative) "Salut tout le monde, je m'appelle Aurélien, j'ai fait un master en Droit.... J'aime bien la société, je vous trouve sympas, je vois un potentiel de fun très grand et j'ai envie de rejoindre le fun."

Maxime J. demande à Aurélien H. ce qu'il pense des côtisations.

Aurélien H. : tant qu'il n'y a pas de dépenses il n'y a pas besoin de côtisations.

Maxime J. demande à Aurélien H. ce qu'il pense du Jass.

Aurélien H. : c'est un jeu sympa mais je ne souhaite pas y jouer maintenant, car je ne joue qu'aux heures paires (il est 20h30)

Pepito demande à Aurélien H. ce qu'il pense apporter à la société.

Aurélien H. : je pense apporter du jeu et du fun. ("Du J & F, comme on dit", répond Pepito)

Arnaud G. remarque que c'est très à propos puisque nos statuts prévoient que le jeu est un de nos buts.

Aurélien H. est accepté au rang d'aspirant par applaudissements de l'assemblée. JD, aspirant assistant à l'AG, est jaloux de ne pas avoir eu droit à une introduction publique similaire.

Maxime J. demande donc à JD ce qu'il pense apporter à la société.

JD : Rien, car Fuck La Présidence. Le jeu.

# Divers

## Design des cartes (Zoé L.)

Zoé L. a commencé le design des cartes et du dos de cartes, et souhaite qu'on lui transmette nos souhaits pour qu'elle n'oublie rien.

Arnaud G. lui demande si elle peut nous montrer l'état actuel de son travail prochainement.

Zoé L. préfère avoir d'abord des inputs, pour pouvoir produire un design qui correspondent à nos besoins avant de présenter une première itération.

Zoé L. listera les choses déjà présentes sur Telegram.

Arnaud G. propose d'ajouter des éléments liés à l'union : band, rameaux d'olivier, ...

Gabriel B. propose d'ajouter des citrons.

_Fin de l'assemblée à 20h40._