---
geometry: margin=2cm
---

![](vialarstyle_avec_liseré_couché_simple.svg){width=3cm}

**Société du Jeu - S.W.V!**

\

# Comptes (déc 2022 → mai 2023)

## Bilan

| Actif | Passif | Titre |
| --- | --- | --- |
| 110.- | | Rubans possédés par la SdJ | 
| | 110.- | Dette pour les rubans envers Maxime J. |

Bilan: 0.-

## Résultat

| Produit | Charge | Titre |
| --- | --- | --- |
| aucun |
 
Résultat: 0.-

## Signature

Le trésorier: \
\

Lieu et date:
